﻿using System.Collections.Generic;
using System.Linq;

using PlayGen.Unity.Utilities.Localization;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TMP_Dropdown))]
public class TMPDropdownLocalization : UILocalization
{
	/// <summary>
	/// The localization keys for this dropdown
	/// </summary>
	[Tooltip("The localization keys for this dropdown")]
	[SerializeField]
	private List<string> _options;

	/// <summary>
	/// Set the dropdown option localization keys
	/// </summary>
	public void SetOptions(List<string> options)
	{
		_options = options;
		Set();
	}

	public override void Set()
	{
		if (_options != null)
		{
			var dropdown = GetComponent<TMP_Dropdown>();
			dropdown.ClearOptions();
			var translatedOptions = _options.Select(t => Localization.Get(t)).ToList();
			dropdown.AddOptions(translatedOptions);
		}
	}
}
