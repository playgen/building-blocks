﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

using PlayGen.Orchestrator.Common;
using PlayGen.Orchestrator.Contracts;
using PlayGen.Orchestrator.PSL.Common.LRS;
using PlayGen.Orchestrator.PSL.Unity.Client;
using PlayGen.Orchestrator.PSL.Unity.Server;
using PlayGen.Unity.AsyncUtilities;
using PlayGen.Unity.ASyncUtilities.WebGL;
using PlayGen.Unity.Utilities.AudioManagement;
using PlayGen.Unity.Utilities.Loading;
using PlayGen.Unity.Utilities.Localization;

using Rewired;
using Rewired.Integration.UnityUI;

using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class PlatformSelection : MonoBehaviour {
	[SerializeField]
	private ConnectionType _connectionType;
	[SerializeField]
	private PlatformManager[] _platformManagers;

	private static PlatformSelection _instance;
	public static ConnectionType ConnectionType
	{
		get { return _instance._connectionType; }
	}
	public static PlatformServerConfig ServerConfig { get; private set; }
	public static int ServerMax = 8;
	public static Action<GameState> ServerStateChanged;
	public static GameState ServerState
	{
		get { return _instance._orchestratedServer ? _instance._orchestratedServer.State : GameState.NotInitialized; }
	}
	public static PlatformClientConfig ClientConfig { get; private set; }
	public static PlayerIdentifier PlayerDetails
	{
		get { return _instance._orchestrationClient.PlayerDetails; }
	}

	private PSLOrchestratedGameServer _orchestratedServer;
	private PSLOrchestrationClient _orchestrationClient;

	private bool _arrowsUsed;
	public static bool Arrows
	{
		get { return _instance._arrowsUsed; }
	}

	void Awake() {
		if (_instance)
		{
			Destroy(gameObject);
			return;
		}
		_instance = this;
		DontDestroyOnLoad(this);
		gameObject.AddComponent<AsyncConfigLoader>();
		if (Application.platform == RuntimePlatform.WebGLPlayer)
		{
			AsyncConfigLoader.Locator = new WebGLStreamingAssetsLocator();
		}
		switch (_connectionType)
		{
			case ConnectionType.Server:
				AsyncConfigLoader.ReadConfig<PlatformServerConfig>("ServerFlags.config.json", OnServerFlagsLoaded, OnFlagError);
				break;
			case ConnectionType.Client:
				AsyncConfigLoader.ReadConfig<PlatformClientConfig>("ClientFlags.config.json", OnClientFlagsLoaded, OnFlagError);
				break;
			case ConnectionType.SelfHosting:
				AsyncConfigLoader.ReadConfig<PlatformServerConfig>("ServerFlags.config.json", OnHostServerFlagsLoaded, OnFlagError);
				break;
		}
	}

	private void OnServerFlagsLoaded(PlatformServerConfig obj)
	{
		ServerConfig = obj;
		var manager = _platformManagers.FirstOrDefault(p => p.ConnectionType == _connectionType);
		if (manager != null && manager.NetworkManagerObj != null)
		{
			Instantiate(manager.NetworkManagerObj);
		}
		_orchestratedServer = FindObjectOfType<PSLOrchestratedGameServer>();
		if (ServerConfig.ListenForServerMessage)
		{
			_orchestratedServer.StateChanged += ServerStateChange;
		}
		_orchestratedServer.RegisteredWithOrchestrator += RegisteredWithOrchestrator;
	}

	private void ServerStateChange(GameState state)
	{
		if (ServerStateChanged != null)
		{
			ServerStateChanged(state);
		}
	}

	public static void EnsureServerState(GameState state)
	{
		if (_instance._orchestratedServer && _instance._orchestratedServer.State < state)
		{
			_instance._orchestratedServer.SetState(state, true);
		}
	}

	public static void UpdateServerState(GameState state)
	{
		if (_instance._orchestratedServer && ServerConfig.ListenForServerMessage)
		{
			_instance._orchestratedServer.SetState(state, true);
		}
	}

	public static void UpdatePlayers(List<string> playerIDs)
	{
		if (_instance._orchestratedServer)
		{
			_instance._orchestratedServer.UpdateConnectedPlayers(playerIDs);
		}
	}

	public static void SendLRSData(string playerId, LRSSkillVerb verb, float value)
	{
		if (_instance._orchestratedServer)
		{
			_instance._orchestratedServer.SendLRSData(playerId, verb, value);
		}
	}

	private void RegisteredWithOrchestrator(GameRegistrationResponse obj)
	{
		var allCultures = CultureInfo.GetCultures(CultureTypes.AllCultures);
		if (allCultures.Any(c => c.Name.Equals(obj.language, StringComparison.OrdinalIgnoreCase)))
		{
			Localization.UpdateLanguage(obj.language);
		}
		else if (allCultures.Any(c => c.EnglishName.Equals(obj.language, StringComparison.OrdinalIgnoreCase)))
		{
			Localization.UpdateLanguage(allCultures.First(c => c.EnglishName.Equals(obj.language, StringComparison.OrdinalIgnoreCase)));
		}
		ServerMax = obj.maxPlayers;
		_orchestratedServer.Config.MinPlayers = obj.maxPlayers;
		ModeSelection.SetDifficulty(obj.difficulty);
		ModeSelection.SetMaxTimeFromMinutes(obj.maxTime);
		ModeSelection.UpdateEnvironment((int)(ModeSelection.EnvironmentMode)Enum.Parse(typeof(ModeSelection.EnvironmentMode), obj.scenario));
	}

	private void OnClientFlagsLoaded(PlatformClientConfig obj)
	{
		var allCultures = CultureInfo.GetCultures(CultureTypes.AllCultures);
		if (!string.IsNullOrEmpty(CultureInfo.CurrentUICulture.Name) && allCultures.Any(c => c.Name.Equals(Application.systemLanguage.ToString(), StringComparison.OrdinalIgnoreCase)))
		{
			Localization.UpdateLanguage(CultureInfo.CurrentUICulture);
		}
		else if (allCultures.Any(c => c.EnglishName.Equals(Application.systemLanguage.ToString(), StringComparison.OrdinalIgnoreCase)))
		{
			Localization.UpdateLanguage(allCultures.First(c => c.EnglishName.Equals(Application.systemLanguage.ToString(), StringComparison.OrdinalIgnoreCase)));
		}
		Loading.Start(Localization.Get("FINDING_MATCH"));
		ClientConfig = obj;
		var manager = _platformManagers.FirstOrDefault(p => p.ConnectionType == _connectionType);
		if (manager != null && manager.NetworkManagerObj != null)
		{
			Instantiate(manager.NetworkManagerObj);
		}
		_orchestrationClient = FindObjectOfType<PSLOrchestrationClient>();
		_orchestrationClient.EndpointError += EndpointError;
	}

	private void EndpointError(EndpointStatus status)
	{
		var errorText = string.Empty;
		switch (status)
		{
			case EndpointStatus.NoInfo:
				errorText = Localization.Get("START_ERROR_GENERIC");
				break;
			case EndpointStatus.TokenFailure:
				errorText = Localization.Get("START_ERROR_INVALID_DETAILS");
				break;
			case EndpointStatus.MatchFailure:
				errorText = Localization.Get("START_ERROR_MATCH_NOT_EXIST");
				break;
			case EndpointStatus.MatchAlreadyJoined:
				errorText = Localization.Get("START_ERROR_ALREADY_JOINED");
				break;
			case EndpointStatus.MatchFull:
				errorText = Localization.Get("START_ERROR_MATCH_FULL");
				break;
		}
		ErrorWindow.ShowError(errorText, false, false);
		Loading.Stop();
	}

	private void OnHostServerFlagsLoaded(PlatformServerConfig obj)
	{
		ServerConfig = obj;
		AsyncConfigLoader.ReadConfig<PlatformClientConfig>("ClientFlags.config.json", OnHostClientFlagsLoaded, OnFlagError);
	}

	private void OnHostClientFlagsLoaded(PlatformClientConfig obj)
	{
		ClientConfig = obj;
		var manager = _platformManagers.FirstOrDefault(p => p.ConnectionType == _connectionType);
		if (manager != null && manager.NetworkManagerObj != null)
		{
			Instantiate(manager.NetworkManagerObj);
		}
		if (!NetworkManager.singleton.isNetworkActive)
		{
			NetworkServer.Reset();
			NetworkManager.singleton.StartHost();
		}
	}

	private void OnFlagError(Exception obj)
	{
		Debug.Log(obj.Message);
	}

	void Start()
	{
		if (_connectionType != ConnectionType.Server)
		{
			AssignAllJoysticksToSystemPlayer(true);
		}
	}

	private void OnEnable()
	{
		SceneManager.activeSceneChanged += SceneChange;
		ReInput.ControllerConnectedEvent += OnControllerConnected;
	}

	private void OnDisable()
	{
		SceneManager.activeSceneChanged -= SceneChange;
		ReInput.ControllerConnectedEvent -= OnControllerConnected;
	}

	private void Update()
	{
		if (!_arrowsUsed && Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow))
		{
			_arrowsUsed = true;
		}
		else if (_arrowsUsed && Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D))
		{
			_arrowsUsed = false;
		}
	}

	private void SceneChange(Scene arg0, Scene arg1)
	{
		if (_connectionType == ConnectionType.Server)
		{
			if (FindObjectOfType<MusicManager>())
			{
				Destroy(FindObjectOfType<MusicManager>().gameObject);
			}
			if (FindObjectOfType<InputManager>())
			{
				Destroy(FindObjectOfType<InputManager>().gameObject);
			}
			if (FindObjectOfType<RewiredStandaloneInputModule>())
			{
				Destroy(FindObjectOfType<RewiredStandaloneInputModule>().gameObject);
			}
		}
		else
		{
			var menuPlayer = ReInput.players.AllPlayers.OrderBy(p => p.id).First(p => p.controllers.hasKeyboard || p.controllers.Controllers.Any()) ?? ReInput.players.GetPlayer(RewiredConsts.Player.Menu_Player);
			FindObjectOfType<RewiredStandaloneInputModule>().RewiredPlayerIds = new[] { menuPlayer.id };
		}
	}

	void OnControllerConnected(ControllerStatusChangedEventArgs args)
	{
		if (args.controllerType != ControllerType.Joystick)
		{
			return;
		}
		ReInput.players.GetSystemPlayer().controllers.AddController<Joystick>(args.controllerId, true);
		if (LocalPlayerList.AssignedJoysticks.Values.Contains(args.controllerId))
		{
			var key = LocalPlayerList.AssignedJoysticks.First(j => j.Value == args.controllerId).Key;
			if (LocalPlayerList.ConnectedPlayers.Contains(key) && !ReInput.players.GetPlayer(key).controllers.Controllers.Any())
			{
				ReInput.players.GetPlayer(key).controllers.AddController<Joystick>(args.controllerId, false);
			}
		}
		if (args.controllerId == 0)
		{
			ReInput.players.GetPlayer(RewiredConsts.Player.Menu_Player).controllers.AddController<Joystick>(args.controllerId, false);
		}
	}

	void AssignAllJoysticksToSystemPlayer(bool removeFromOtherPlayers)
	{
		foreach (var j in ReInput.controllers.Joysticks)
		{
			ReInput.players.GetSystemPlayer().controllers.AddController<Joystick>(j.id, removeFromOtherPlayers);
			if (j.id == 0)
			{
				ReInput.players.GetPlayer(RewiredConsts.Player.Menu_Player).controllers.AddController<Joystick>(j.id, false);
			}
		}
	}
}
