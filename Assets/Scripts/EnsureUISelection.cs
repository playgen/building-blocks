﻿using System.Linq;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EnsureUISelection : MonoBehaviour {

	private GameObject _lastSelected;
	[SerializeField]
	private GameObject _defaultSelected;

	private void OnEnable()
	{
		GetComponentsInChildren<Selectable>(true).Where(s => s.targetGraphic != null).ToList().ForEach(s => s.targetGraphic.raycastTarget = Application.isMobilePlatform);
		if (!Application.isMobilePlatform)
		{
			Invoke("EnableSelect", 0f);
		}
	}

	private void EnableSelect()
	{
		if (EventSystem.current)
		{
			if (_defaultSelected.activeSelf && _defaultSelected.GetComponent<Selectable>().IsInteractable())
			{
				EventSystem.current.SetSelectedGameObject(null);
				EventSystem.current.SetSelectedGameObject(_defaultSelected);
			}
			if (EventSystem.current.currentSelectedGameObject && EventSystem.current.currentSelectedGameObject.transform.IsChildOf(transform))
			{
				_lastSelected = EventSystem.current.currentSelectedGameObject;
			}
		}
	}

	public void SetDefault(GameObject obj)
	{
		_defaultSelected = obj;
	}

	void Update () {
		if (!Application.isMobilePlatform)
		{
			if (!GetComponent<ErrorWindow>() && ErrorWindow.IsVisible)
			{
				return;
			}
			if (!_lastSelected)
			{
				_lastSelected = null;
			}
			if (EventSystem.current)
			{
				if (!EventSystem.current.currentSelectedGameObject || !EventSystem.current.currentSelectedGameObject.activeInHierarchy ||
					!EventSystem.current.currentSelectedGameObject.GetComponentInChildren<Selectable>().interactable || !EventSystem.current.currentSelectedGameObject.transform.IsChildOf(transform))
				{
					var objToSelect = _lastSelected ?? _defaultSelected;
					if (objToSelect.activeInHierarchy && objToSelect.GetComponent<Selectable>().IsInteractable())
					{
						EventSystem.current.SetSelectedGameObject(null);
						EventSystem.current.SetSelectedGameObject(objToSelect);
					}
				}
				else if (_lastSelected != EventSystem.current.currentSelectedGameObject && EventSystem.current.currentSelectedGameObject.transform.IsChildOf(transform))
				{
					_lastSelected = EventSystem.current.currentSelectedGameObject;
				}
			}
		}
	}
}
