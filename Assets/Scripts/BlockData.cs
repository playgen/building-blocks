﻿using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.Networking;

public class BlockData : NetworkBehaviour {
	struct SyncColor
	{
		public readonly float r;
		public readonly float g;
		public readonly float b;

		public SyncColor (float red, float green, float blue)
		{
			r = red;
			g = green;
			b = blue;
		}
	}
	class SyncListColor : SyncListStruct<SyncColor>
	{
	}

	[SyncVar]
	private bool _locked;
	[SyncVar]
	private Color _unlockedColor;
	[SyncVar]
	private bool _flashing;
	private readonly SyncListColor _lockedColor = new SyncListColor();

	public bool Locked
	{
		get { return _locked; }
	}

	public Color UnlockedColor
	{
		get { return _unlockedColor; }
	}

	public bool Flashing
	{
		get { return _flashing; }
	}

	public List<Color> LockedColor
	{
		get
		{
			var colorList = new List<Color>();
			foreach (var color in _lockedColor)
			{
				colorList.Add(new Color(color.r, color.g, color.b));
			}
			return colorList;
		}
	}

	private void Start()
	{
		var colorSprites = GetComponentsInChildren<SpriteRenderer>().Where(s => s.color == Color.white).ToList();
		if (Locked)
		{
			for (var i = 0; i < colorSprites.Count; i++)
			{
				colorSprites[i].color = LockedColor[i];
			}
		}
		else
		{
			GetComponentsInChildren<SpriteRenderer>().Where(s => s.color == Color.white).ToList().ForEach(s => s.color = UnlockedColor);
		}
		GetComponentsInChildren<ParticleSystem>().ToList().ForEach(p => p.Play());
	}

	public void Setup(Color unlocked, Color[] locked)
	{
		_unlockedColor = unlocked;
		foreach (var color in locked)
		{
			var syncColor = new SyncColor(color.r, color.g, color.b);
			_lockedColor.Add(syncColor);
		}
	}

	public void Lock()
	{
		_locked = true;
		_flashing = true;
		Invoke("StopFlash", 0.05f);
	}

	private void StopFlash()
	{
		_flashing = false;
	}
}
