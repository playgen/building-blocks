﻿using UnityEngine;
using UnityEngine.Networking;

public class PlayerSfxManager : NetworkBehaviour {

	private AudioSource _audio;
	[SerializeField]
	private AudioClip _jump;
	[SerializeField]
	private AudioClip _rotate;
	[SerializeField]
	private AudioClip _rotateFail;
	[SerializeField]
	private AudioClip _lock;
	[SerializeField]
	private AudioClip _spawn;
	[SerializeField]
	private AudioClip _shapeSelect;

	private void Awake()
	{
		_audio = GetComponent<AudioSource>();
		_audio.volume = PlayerPrefs.GetFloat("Sound Volume", 1) * 0.5f;
	}

	[ClientRpc]
	public void RpcJump()
	{
		_audio.Stop();
		_audio.clip = _jump;
		_audio.Play();
	}

	[ClientRpc]
	public void RpcRotate()
	{
		_audio.Stop();
		_audio.clip = _rotate;
		_audio.Play();
	}

	[ClientRpc]
	public void RpcRotateFail()
	{
		_audio.Stop();
		_audio.clip = _rotateFail;
		_audio.Play();
	}

	[ClientRpc]
	public void RpcLock()
	{
		_audio.Stop();
		_audio.clip = _lock;
		_audio.Play();
	}

	[ClientRpc]
	public void RpcSpawn()
	{
		_audio.Stop();
		_audio.clip = _spawn;
		_audio.Play();
	}

	[ClientRpc]
	public void RpcSelect()
	{
		_audio.Stop();
		_audio.clip = _shapeSelect;
		_audio.Play();
	}

	public void UpdateVolume()
	{
		_audio.volume = PlayerPrefs.GetFloat("Sound Volume", 1) * 0.5f;
	}
}
