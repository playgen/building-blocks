﻿using UnityEngine;
using UnityEngine.Networking;

public class ClientNetworkManager : NetworkManager {

	public override void OnClientError(NetworkConnection conn, int errorCode)
	{
		base.OnClientError(conn, errorCode);
		Debug.LogError(errorCode);
		ErrorWindow.ShowError("Error with connection to game. Error code: " + errorCode, true, false);
	}
}
