﻿using System.Collections.Generic;

public static class LocalPlayerList {

	public static List<int> ConnectedPlayers = new List<int>();
	public static Dictionary<int, int> AssignedJoysticks = new Dictionary<int, int>();
	public static Dictionary<int, int> PlayerSelection = new Dictionary<int, int>();
	public static Dictionary<int, int> PlayerSide = new Dictionary<int, int>();
	public static Dictionary<int, bool> PlayerIsSelecting = new Dictionary<int, bool>();
}
