﻿using System;

[Serializable]
public class PlatformClientConfig {

	public bool DisplayMainMenuUI;
	public bool AutoJoinGame;
	public bool DisplayControlsAtStart;
	public bool DisplaySocialInstructionsAtStart;
	public bool AllowRestarting;
	public bool AllowQuitting;
}
