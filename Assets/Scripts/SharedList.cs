﻿using UnityEngine;

public class SharedList : MonoBehaviour {

	public GameObject[] Shapes;
	public Color[] Colors;
	public int[] ShapeHeight;
	public float[] ShapeJumpLeft;
	public float[] ShapeJumpRight;
}
