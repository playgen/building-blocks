﻿using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class ErrorWindow : MonoBehaviour {

	private static ErrorWindow _instance;
	[SerializeField]
	private TextMeshProUGUI _errorText;
	[SerializeField]
	private Button _closeButton;

	public static bool IsVisible
	{
		get { return _instance && _instance.GetComponent<CanvasGroup>().interactable; }
	}

	private void Awake()
	{
		if (_instance)
		{
			Destroy(gameObject);
			return;
		}
		_instance = this;
		DontDestroyOnLoad(this);
		Visible(false);
	}

	public static void ShowError(string errorString, bool disconnect, bool reload)
	{
		_instance.Visible(true);
		_instance._errorText.text = errorString;
		_instance._closeButton.onClick.RemoveAllListeners();
		_instance._closeButton.onClick.AddListener(delegate { _instance.Visible(false); });
		if (disconnect)
		{
			if (PlatformSelection.ConnectionType == ConnectionType.Client)
			{
				NetworkManager.singleton.StopClient();
			}
			else
			{
				NetworkManager.singleton.ServerChangeScene(NetworkManager.singleton.onlineScene);
			}
		}
		if (reload)
		{
			SceneManager.LoadScene(NetworkManager.singleton ? NetworkManager.singleton.offlineScene : SceneManager.GetActiveScene().name);
		}
	}

	public void Visible(bool visible)
	{
		GetComponent<EnsureUISelection>().enabled = visible;
		GetComponent<CanvasGroup>().alpha = visible ? 0.75f : 0;
		GetComponent<CanvasGroup>().interactable = visible;
		GetComponent<CanvasGroup>().blocksRaycasts = visible;
	}
}

#if UNITY_EDITOR
[CustomEditor(typeof(ErrorWindow))]
public class ErrorWindowEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		if (Application.isPlaying)
		{
			if (GUILayout.Button("Error"))
			{
				ErrorWindow.ShowError("Test", true, true);
			}
		}
	}
}
#endif