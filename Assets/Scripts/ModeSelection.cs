﻿using System;

using PlayGen.Unity.Utilities.Localization;

using UnityEngine;

public class ModeSelection : MonoBehaviour {
	public enum EnvironmentMode
	{
		Default,
		Seesaw
	}

	public enum SupplyMode
	{
		PerBlock,
		Shared
	}

	private static ModeSelection _instance;
	[SerializeField]
	private EnvironmentMode _environmentMode;
	[SerializeField]
	private SupplyMode _supplyMode;
	[SerializeField]
	private bool _earthquakesEnabled;
	[SerializeField]
	private bool _flappyEnabled;
	[SerializeField]
	private int _difficulty = 2;
	[SerializeField]
	private int _maxCheckpoints;
	[SerializeField]
	private int _maxTime;

	public static EnvironmentMode Environment
	{
		get { return _instance._environmentMode; }
	}
	public static SupplyMode Supply
	{
		get { return _instance._supplyMode; }
	}
	public static bool Earthquakes
	{
		get { return _instance._earthquakesEnabled; }
	}
	public static bool Flappy
	{
		get { return _instance._flappyEnabled; }
	}
	public static int Difficulty
	{
		get { return _instance._difficulty; }
	}
	public static int MaxCheckpoints
	{
		get { return _instance._maxCheckpoints; }
	}
	public static int MaxTime
	{
		get { return _instance._maxTime; }
	}

	void Awake()
	{
		if (_instance)
		{
			Destroy(gameObject);
			return;
		}
		_instance = this;
		DontDestroyOnLoad(this);
	}

	public static void UpdateEnvironment(int change)
	{
		var index = ((int)_instance._environmentMode) + change;
		if (index < 0)
		{
			index = Enum.GetNames(typeof(EnvironmentMode)).Length - 1;
		}
		if (index >= Enum.GetNames(typeof(EnvironmentMode)).Length)
		{
			index = 0;
		}
		_instance._environmentMode = (EnvironmentMode)index;
	}

	public static void UpdateSupply(int change)
	{
		var index = ((int)_instance._supplyMode) + change;
		if (index < 0)
		{
			index = Enum.GetNames(typeof(SupplyMode)).Length - 1;
		}
		if (index >= Enum.GetNames(typeof(SupplyMode)).Length)
		{
			index = 0;
		}
		_instance._supplyMode = (SupplyMode)index;
	}

	public static void UpdateEarthquakes(bool enable)
	{
		_instance._earthquakesEnabled = enable;
	}

	public static void UpdateFlappy(bool enable)
	{
		_instance._flappyEnabled = enable;
	}

	public static void SetDifficulty(int diff)
	{
		_instance._difficulty = diff;
	}

	public static void SetMaxCheckpoints(int max)
	{
		_instance._maxCheckpoints = max;
	}

	public static void SetMaxTimeFromMinutes(float max)
	{
		_instance._maxTime = (int)(max * 60);
	}

	public static void SetMaxTime(int max)
	{
		_instance._maxTime = max;
	}

	public static string ModeString(bool withSpaces = true)
	{
		var str = string.Empty;
		switch (_instance._supplyMode)
		{
			case SupplyMode.PerBlock:
				str += Localization.Get("MODE_STRING_SUPPLY_PER_BLOCK");
				break;
			case SupplyMode.Shared:
				str += Localization.Get("MODE_STRING_SHARED_SUPPLY");
				break;
		}
		switch (_instance._environmentMode)
		{
			case EnvironmentMode.Seesaw:
				str += Localization.Get("MODE_STRING_WITH_SEESAW");
				break;
		}
		if (_instance._earthquakesEnabled)
		{
			if (_instance._environmentMode != EnvironmentMode.Default)
			{
				if (_instance._flappyEnabled)
				{
					str += Localization.Get("MODE_STRING_ALSO_EARTHQUAKES");
				}
				else
				{
					str += Localization.Get("MODE_STRING_AND_EARTHQUAKES");
				}
			}
			else
			{
				str += Localization.Get("MODE_STRING_WITH_EARTHQUAKES");
			}
		}
		if (_instance._flappyEnabled)
		{
			if (_instance._environmentMode != EnvironmentMode.Default || _instance._earthquakesEnabled)
			{
				str += Localization.Get("MODE_STRING_AND_FLAPPY");
			}
			else
			{
				str += Localization.Get("MODE_STRING_WITH_FLAPPY");
			}
		}
		if (_instance._maxTime > 0)
		{
			str += " (" + Localization.GetAndFormat("MODE_TIME_SET", false, (_instance._maxTime / 60)) + ")";
		}
		if (!withSpaces)
		{
			str = str.Replace(" ", string.Empty);
			str = str.Replace("(", string.Empty);
			str = str.Replace(")", string.Empty);
		}
		return str;
	}
}
