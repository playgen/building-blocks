﻿using System;
using System.Collections;

using UnityEngine;

public class CameraAspectSet : MonoBehaviour
{
	private Vector2 _previousResolution;
	private Vector3 _initialPosition;
	private Vector3 _camOffset;
	private Camera _cam;
	[SerializeField]
	private SpriteRenderer _background;
	private float _zoomAmount = 1;

	private void Awake()
	{
		_cam = Camera.main;
		_initialPosition = _cam.transform.position;
		SetCameraSize();
		AudioListener.volume = PlayerPrefs.GetFloat("Master Volume", 1);
		if (Application.platform == RuntimePlatform.WindowsPlayer)
		{
			Cursor.lockState = CursorLockMode.Locked;
		}
	}

	private void OnApplicationFocus(bool focus)
	{
		if (focus && Application.platform == RuntimePlatform.WindowsPlayer && PlatformSelection.ConnectionType == ConnectionType.SelfHosting)
		{
			Cursor.lockState = CursorLockMode.Locked;
		}
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.PageUp))
		{
			Application.CaptureScreenshot("Screenshot" + DateTime.UtcNow.ToFileTimeUtc() + ".jpg");
		}
	}

	private void LateUpdate()
	{
		if (!Mathf.Approximately(_previousResolution.x, Screen.width) || !Mathf.Approximately(_previousResolution.y, Screen.height))
		{
			SetCameraSize();
		}
	}

	public void UpdateZoom(float zoomAmount)
	{
		_zoomAmount = zoomAmount;
		SetCameraSize();
	}

	public void SetCameraSize()
	{
		_previousResolution = new Vector2(Screen.width, Screen.height);
		var aspect = _previousResolution.x / _previousResolution.y;
		_cam.orthographicSize = 16 * (2f / aspect) * _zoomAmount;
		transform.position = new Vector3(0, _cam.orthographicSize - 16, 0) + _initialPosition + _camOffset;
		_background.size = new Vector2(64, _cam.orthographicSize * 2);
	}

	public IEnumerator ScreenShake(int level)
	{
		var shakeTime = 5f;
		while (shakeTime > 0)
		{
			_camOffset = UnityEngine.Random.insideUnitSphere * 0.02f * level;
			shakeTime -= Time.smoothDeltaTime;
			SetCameraSize();
			yield return new WaitForEndOfFrame();
		}
		_camOffset = Vector3.zero;
		SetCameraSize();
	}
}