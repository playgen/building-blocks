﻿using System;
using UnityEngine;

public static class CollisionExtensions {

	private static readonly Collider2D[] _colliders = new Collider2D[25];
	private static readonly ContactPoint2D[] _contacts = new ContactPoint2D[25];
	private static ContactFilter2D _layers = new ContactFilter2D { layerMask = Physics2D.DefaultRaycastLayers };

	public static Collider2D[] Colliders
	{
		get { return _colliders; }
	}
	public static ContactPoint2D[] Contacts
	{
		get { return _contacts; }
	}

	public static void GetOverlappingColliders(this Collider2D collider, LayerMask layers)
	{
		_layers = new ContactFilter2D { layerMask = layers };
		Array.Clear(_colliders, 0, 25);
		collider.OverlapCollider(_layers, _colliders);
	}

	public static void GetOverlappingColliders(this Rigidbody2D rigidbody, LayerMask layers)
	{
		_layers = new ContactFilter2D { layerMask = layers };
		Array.Clear(_colliders, 0, 25);
		rigidbody.OverlapCollider(_layers, _colliders);
	}

	public static void GetOverlapsInArea(Vector2 pointA, Vector2 pointB, LayerMask layers)
	{
		_layers = new ContactFilter2D { layerMask = layers };
		Array.Clear(_colliders, 0, 25);
		Physics2D.OverlapArea(pointA, pointB, _layers, _colliders);
	}

	public static void GetContactPoints(this Rigidbody2D rigidbody, LayerMask layers)
	{
		_layers = new ContactFilter2D { layerMask = layers };
		Array.Clear(_contacts, 0, 25);
		rigidbody.GetContacts(_contacts);
	}
}
