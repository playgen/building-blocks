﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

using Random = UnityEngine.Random;

public class PlayerController : NetworkBehaviour {

	[SerializeField]
	private float _maxSpeed;
	[SerializeField]
	private float _maxRotateSpeed;
	[SerializeField]
	private float _jumpForce;
	[SerializeField]
	private int _maxJumpCount;
	private int _jumpCount;
	private bool _jump;
	private float _moveDir;
	private int _rotateDir;
	private bool _isRotating;
	private BoxCollider2D[] _groundCheck;
	private bool _grounded;
	private readonly List<GameObject> _currentFloors = new List<GameObject>();
	private Rigidbody2D _rigidbody2D;
	private SpriteRenderer _shapeSprite;
	private Transform _previousSpriteTransform;
	private float _currentRotation;
	private Vector2 _startPos = new Vector2(30f, -16);
	private readonly List<BlockData> _knownBlocks = new List<BlockData>();
	private GameLogicControl _logic;
	private PlayerSfxManager _sfxManager;

	[SyncVar]
	private int _id;
	[SyncVar]
	private Color _color;
	[SyncVar]
	private bool _moving;
	[SyncVar]
	private bool _selecting = true;
	[SyncVar]
	private string _name;
	private string _sessionPlayerID;

	public int ID
	{
		get { return _id; }
	}

	public string SessionPlayerID
	{
		get { return _sessionPlayerID; }
	}

	public string PlayerName
	{
		get { return _name; }
	}

	public bool Moving
	{
		get { return _moving; }
	}

	public bool Selecting
	{
		get { return _selecting; }
	}

	private void Start()
	{
		if (!hasAuthority)
		{
			Destroy(GetComponent<PlayerInput>());
		}
		else if (PlatformSelection.ConnectionType == ConnectionType.Client)
		{
			CmdSetName(PlatformSelection.PlayerDetails.Id.ToString(), PlatformSelection.PlayerDetails.Name);
		}
		_sfxManager = GetComponent<PlayerSfxManager>();
	}

	[Command]
	private void CmdSetName(string playerID, string playerName)
	{
		_name = playerName;
		_sessionPlayerID = playerID;
		Debug.Log(_name);
	}

	public void SetID(GameLogicControl logic, int id, Color color)
	{
		_logic = logic;
		_id = id;
		_color = color;
	}

	private void FixedUpdate()
	{
		if (isClient)
		{
			FixedUpdateClient();
		}
		if (isServer)
		{
			FixedUpdateServer();
		}
	}

	[Server]
	private void FixedUpdateServer()
	{
		if (_rigidbody2D)
		{
			_grounded = false;
			_currentFloors.Clear();
			foreach (var col in _groundCheck)
			{
				if (col.bounds.extents.x > col.bounds.extents.y * 3 && _rigidbody2D.OverlapPoint(new Vector2(0, 0.05f) + (Vector2)col.transform.position))
				{
					col.GetOverlappingColliders(Physics2D.DefaultRaycastLayers);
					foreach (var t in CollisionExtensions.Colliders)
					{
						if (t != null)
						{
							if (t.gameObject != _rigidbody2D.gameObject && t.gameObject.layer != col.gameObject.layer &&
								(t.gameObject.layer == LayerMask.NameToLayer("Player") || t.gameObject.layer == LayerMask.NameToLayer("PlayerFloor") ||
								t.gameObject.layer == LayerMask.NameToLayer("Block") || t.gameObject.layer == LayerMask.NameToLayer("Seesaw")))
							{
								_rigidbody2D.GetContactPoints(Physics2D.DefaultRaycastLayers);
								if (CollisionExtensions.Contacts.Where(c => c.collider == t).Any(c => c.normal.y > 0.5f))
								{
									_grounded = true;
									if (!_currentFloors.Contains(t.gameObject))
									{
										_currentFloors.Add(t.gameObject);
									}
								}
								if (!_grounded)
								{
									var lowColliders = CollisionExtensions.Contacts.Where(c => c.collider != null && c.normal.y >= 0f && c.normal.y <= 0.5f).ToList();
									if (lowColliders.Count > 1 && lowColliders.Max(c => c.normal.x) - lowColliders.Min(c => c.normal.x) > 1.6f)
									{
										_grounded = true;
									}
									else
									{
										var nonLow = CollisionExtensions.Contacts.Where(c => c.collider != null && !lowColliders.Contains(c)).ToList();
										foreach (var lowCol in lowColliders)
										{
											foreach (var nonLowCol in nonLow)
											{
												if (nonLowCol.normal.y > -0.0001f && nonLowCol.normal.y < 0f)
												{
													if (lowCol.normal.x > 0 ? nonLowCol.normal.x < -0.999f : nonLowCol.normal.x > 0.999f)
													{
														_grounded = true;
														break;
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			Move(_moveDir, _rotateDir, _jump);
			if (_grounded && _jumpCount < _maxJumpCount - (_jump ? 1 : 0))
			{
				_jumpCount = _maxJumpCount;
			}
			_jump = false;
			_rotateDir = 0;
		}
	}

	[Server]
	private void ServerCreateShape()
	{
		if (_rigidbody2D)
		{
			return;
		}
		var shape = _logic.GetShape(LocalPlayerList.PlayerSelection[ID]);
		if (shape != null)
		{
			var spawnPos = new Vector2(_startPos.x * (LocalPlayerList.PlayerSide[_id] == 0 ? -1 : 1), ModeSelection.Flappy ? 0 : _startPos.y);
			CollisionExtensions.GetOverlapsInArea(new Vector2(spawnPos.x - 2, 16), new Vector2(spawnPos.x + 2, ModeSelection.Flappy ? 0 : -16), LayerMask.NameToLayer("Player"));
			var playerHighestList = CollisionExtensions.Colliders.ToList().Where(s => s != null && s.gameObject.layer == LayerMask.NameToLayer("Player")).ToList();
			var playerHighest = playerHighestList.Count > 0 ? playerHighestList.Max(s => s.transform.Find("Shape Sprite").GetComponent<Renderer>().bounds.max.y) : spawnPos.y;

			var newShape = Instantiate(shape, transform, false);
			newShape.name = shape.name;
			_shapeSprite = newShape.transform.Find("Shape Sprite").GetComponent<SpriteRenderer>();
			newShape.transform.position = new Vector2(spawnPos.x, playerHighest + _shapeSprite.bounds.extents.y * (ModeSelection.Flappy ? 2 : 1));
			_rigidbody2D = GetComponentInChildren<Rigidbody2D>();
			_moving = true;
			_rigidbody2D.constraints = RigidbodyConstraints2D.FreezeRotation;
			_groundCheck = _rigidbody2D.transform.GetComponentsInChildren<BoxCollider2D>().Where(c => c.gameObject != _rigidbody2D.gameObject).ToArray();
			_rigidbody2D.GetComponent<Collider2D>().density = 1;
			_rigidbody2D.gameObject.layer = LayerMask.NameToLayer("Player");
			newShape.GetComponent<BlockData>().Setup(_color, newShape.GetComponentsInChildren<SpriteRenderer>().Where(s => s.color == Color.white).ToList().Select(s => Random.ColorHSV(0, 0.95f, 0.5f, 0.95f, 0.7f, 0.95f)).ToArray());
			newShape.GetComponentsInChildren<SpriteRenderer>().Where(s => s.color == Color.white).ToList().ForEach(s => s.color = newShape.GetComponent<BlockData>().UnlockedColor);
			NetworkServer.Spawn(newShape);
			RpcSyncBlocks();
			_sfxManager.RpcSpawn();
			_logic.UpdateShownPointers(_id, LocalPlayerList.PlayerSelection[_id], false);
			_logic.UpdatePlayersSelecting(_id, false);
			if (ModeSelection.Flappy)
			{
				_moveDir = LocalPlayerList.PlayerSide[_id] == 0 ? 1 : -1;
				_jumpCount = _maxJumpCount;
				_rigidbody2D.drag = 0;
				_rigidbody2D.angularDrag = 0;
				_rigidbody2D.gravityScale = 4;
			}
			RpcSendClientObject(newShape);
		}
	}

	[ClientRpc]
	private void RpcSendClientObject(GameObject newShape)
	{
		_shapeSprite = newShape.transform.Find("Shape Sprite").GetComponent<SpriteRenderer>();
		_rigidbody2D = newShape.GetComponent<Rigidbody2D>();
		_currentRotation = 0;
	}

	[Server]
	private void Move(float move, float rotate, bool jump)
	{
		var lowestGround = -1f;
		var verticalPush = 0f;
		var angleAmount = 0;
		var hittingWall = false;

		//todo: anti-cheat for going off screen with glitch
		//todo: will need changing if/when flappy is available online
		if (ModeSelection.Flappy)
		{
			if (!_shapeSprite.isVisible)
			{
				ServerUpdatePosition(RewiredConsts.Action.Lock);
				return;
			}
		}

		foreach (var col in _groundCheck)
		{
			if (col.bounds.extents.y > col.bounds.extents.x * 3)
			{
				col.GetOverlappingColliders(Physics2D.DefaultRaycastLayers);
				foreach (var t in CollisionExtensions.Colliders)
				{
					if (t != null)
					{
						if (ModeSelection.Flappy)
						{
							if (t.gameObject != _rigidbody2D.gameObject && t.gameObject.transform.parent != _rigidbody2D.transform)
							{
								if (t.gameObject.layer == LayerMask.NameToLayer("Player") || t.gameObject.layer == LayerMask.NameToLayer("Block") ||
									t.gameObject.layer == LayerMask.NameToLayer("PlayerFloor") || t.gameObject.layer == LayerMask.NameToLayer("Seesaw"))
								{
									ServerUpdatePosition(RewiredConsts.Action.Lock);
									return;
								}
								if (!hittingWall)
								{
									if (t.gameObject.layer == LayerMask.NameToLayer("Wall"))
									{
										_moveDir *= -1;
										move *= -1;
										hittingWall = true;
									}
								}
							}
						}
						else
						{
							if (_currentFloors.Contains(t.gameObject))
							{
								var angle = (move < 0 ? 360 - t.gameObject.transform.eulerAngles.z : t.gameObject.transform.eulerAngles.z) % 90f;
								if (_rigidbody2D.OverlapPoint(new Vector2((move > 0 ? -1 : 1) - move * 0.05f, 0) + (Vector2)col.transform.position))
								{
									verticalPush += angle;
									angleAmount++;
								}
								if (angle < lowestGround || Mathf.Approximately(lowestGround, -1f))
								{
									lowestGround = angle;
								}
							}
							else if (!_grounded && t.gameObject != _rigidbody2D.gameObject &&
									(t.gameObject.layer == LayerMask.NameToLayer("Player") || t.gameObject.layer == LayerMask.NameToLayer("Block") ||
									t.gameObject.layer == LayerMask.NameToLayer("Wall") || t.gameObject.layer == LayerMask.NameToLayer("Seesaw")))
							{
								hittingWall = true;
							}
						}
					}
				}
			}
		}

		if (Mathf.Abs(move) > 0.1f)
		{
			if (angleAmount > 0)
			{
				verticalPush /= angleAmount;
			}
			if (verticalPush > 45)
			{
				verticalPush = 45;
			}
			if (hittingWall && !Mathf.Approximately(verticalPush, 0))
			{
				hittingWall = false;
			}

			if (!ModeSelection.Flappy)
			{
				//StuckCheck(move, 0);
				_rigidbody2D.AddForce(new Vector2(move * _maxSpeed * (_grounded ? 1 : hittingWall ? 0 : 0.6f), hittingWall ? 0 : verticalPush));
			}
			else
			{
				if (Mathf.Abs(_rigidbody2D.velocity.x) < 1)
				{
					_rigidbody2D.AddForce(new Vector2(move * _maxSpeed * 100f, 0));
				}
			}
		}

		if (!_isRotating && Mathf.Abs(rotate) > 0.1f)
		{
			var startAngle = _rigidbody2D.transform.eulerAngles.z;
			_rigidbody2D.transform.Rotate(new Vector3(0, 0, (rotate < 0 ? 1 : -1) * _maxRotateSpeed));
			var valid = true;
			_rigidbody2D.GetOverlappingColliders(Physics2D.DefaultRaycastLayers);
			foreach (var t in CollisionExtensions.Colliders)
			{
				if (t != null)
				{
					if (t.transform.parent != null && t.transform.parent.gameObject != _rigidbody2D.gameObject && t.gameObject.layer != LayerMask.NameToLayer("Checkpoint"))
					{
						valid = false;
						_rigidbody2D.transform.Rotate(new Vector3(0, 0, (rotate < 0 ? -1 : 1) * _maxRotateSpeed));
						break;
					}
				}
			}
			if (valid)
			{
				RpcSmoothRotation(startAngle, _rigidbody2D.transform.eulerAngles.z);
				_sfxManager.RpcRotate();
			}
			else
			{
				for (int i = 0; i < _maxRotateSpeed; i++)
				{
					_rigidbody2D.transform.Rotate(new Vector3(0, 0, rotate < 0 ? 1 : -1));
					_rigidbody2D.GetOverlappingColliders(Physics2D.DefaultRaycastLayers);
					var collide = false;
					foreach (var t in CollisionExtensions.Colliders)
					{
						if (t != null)
						{
							if (t.transform.parent != null && t.transform.parent.gameObject != _rigidbody2D.gameObject && t.gameObject.layer != LayerMask.NameToLayer("Checkpoint"))
							{
								_rigidbody2D.transform.Rotate(new Vector3(0, 0, (rotate < 0 ? -1 : 1) * (i + 1)));
								RpcFailedRotation((rotate < 0 ? 1 : -1) * i + 1);
								collide = true;
								break;
							}
						}
					}
					if (collide)
					{
						break;
					}
				}
				_sfxManager.RpcRotateFail();
			}
		}

		if (jump)
		{
			if (!ModeSelection.Flappy)
			{
				//StuckCheck(0, 1);
				_jumpCount--;
			}
			_rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x, 0);
			_rigidbody2D.AddForce(new Vector2(0f, _jumpForce * (ModeSelection.Flappy ? 5 : 1)));
			_sfxManager.RpcJump();
			RpcAnimateJump();
		}
	}

	[Server]
	private void StuckCheck(float x, float y)
	{
		if (_grounded)
		{
			_rigidbody2D.GetContactPoints(Physics2D.DefaultRaycastLayers);
			foreach (var contact in CollisionExtensions.Contacts.Where(c => c.collider != null && Mathf.FloorToInt(Mathf.Abs(c.normal.x * 100)) == 70 && Mathf.FloorToInt(Mathf.Abs(c.normal.y * 100)) == 70))
			{
				var movement = _rigidbody2D.position - contact.point;
				movement = new Vector2(Mathf.Clamp(movement.x, -0.025f, 0.025f), Mathf.Clamp(movement.y, -0.025f, 0.025f));
				if ((Mathf.Approximately(x, 0) || (x < 0 == movement.x < 0)) && y >= 0 == movement.y > 0)
				{
					_rigidbody2D.MovePosition(_rigidbody2D.position + movement);
				}
			}
		}
	}

	[ClientRpc]
	void RpcAnimateJump()
	{
		if (_rigidbody2D && _shapeSprite)
		{
			StartCoroutine(AnimateJump(Mathf.RoundToInt(_rigidbody2D.transform.eulerAngles.z / _maxRotateSpeed)));
		}
	}

	[Client]
	IEnumerator AnimateJump(int rotation)
	{
		var spriteTransform = _shapeSprite.transform;
		var rbRef = _rigidbody2D;
		var spriteCollider = spriteTransform.GetComponent<Collider2D>();
		var hasCollided = false;
		var timer = 0f;
		var newScale = rotation % 2 != 0 ? new Vector3(1.5f, 0.666f, 1f) : new Vector3(0.666f, 1.5f, 1f);
		var scale = rbRef.GetComponent<Collider2D>().bounds.size.y * 0.5f;
		while (timer < 1f && spriteTransform.gameObject != null)
		{
			timer += Time.smoothDeltaTime * 100;
			yield return new WaitForEndOfFrame();
		}
		timer = 0f;
		while (!hasCollided && timer < 1f && spriteTransform.gameObject != null)
		{
			timer += Time.smoothDeltaTime * 50;
			spriteTransform.localScale = Vector3.Lerp(Vector3.one, newScale, timer);
			if (rotation >= 2)
			{
				spriteTransform.localPosition = rotation % 2 != 0 ? new Vector3((spriteTransform.localScale.x - 1) * -scale, 0, 0) : new Vector3(0, (spriteTransform.localScale.y - 1) * -scale, 0);
			}
			else
			{
				spriteTransform.localPosition = rotation % 2 != 0 ? new Vector3((spriteTransform.localScale.x - 1) * scale, 0, 0) : new Vector3(0, (spriteTransform.localScale.y - 1) * scale, 0);
			}
		   
			spriteCollider.GetOverlappingColliders(Physics2D.DefaultRaycastLayers);
			var validColliders = CollisionExtensions.Colliders.Where(c => c != null && c.transform.parent != rbRef.transform && c.gameObject != rbRef.gameObject).ToList();
			if (validColliders.Any(c => c.gameObject.layer == LayerMask.NameToLayer("Player") ||
										c.gameObject.layer == LayerMask.NameToLayer("Block") ||
										c.gameObject.layer == LayerMask.NameToLayer("Seesaw")))
			{
				hasCollided = true;
			}
			yield return new WaitForEndOfFrame();
		}
		var peakSize = Vector3.Lerp(Vector3.one, newScale, timer);
		var returnSpeed = 1 / timer;
		timer = 0f;
		while (timer < 1f && spriteTransform.gameObject != null)
		{
			timer += Time.smoothDeltaTime * 10 * returnSpeed;
			spriteTransform.localScale = Vector3.Lerp(peakSize, Vector3.one, timer);
			if (rotation >= 2)
			{
				spriteTransform.localPosition = rotation % 2 != 0 ? new Vector3((spriteTransform.localScale.x - 1) * -scale, 0, 0) : new Vector3(0, (spriteTransform.localScale.y - 1) * -scale, 0);
			}
			else
			{
				spriteTransform.localPosition = rotation % 2 != 0 ? new Vector3((spriteTransform.localScale.x - 1) * scale, 0, 0) : new Vector3(0, (spriteTransform.localScale.y - 1) * scale, 0);
			}
			if (!hasCollided)
			{
				spriteCollider.GetOverlappingColliders(Physics2D.DefaultRaycastLayers);
				var validColliders = CollisionExtensions.Colliders.Where(c => c != null && c.transform.parent != rbRef.transform && c.gameObject != rbRef.gameObject).ToList();
				if (validColliders.Any(c => c.gameObject.layer == LayerMask.NameToLayer("Player") ||
											c.gameObject.layer == LayerMask.NameToLayer("Block") ||
											c.gameObject.layer == LayerMask.NameToLayer("Seesaw")))
				{
					hasCollided = true;
				}
			}
			yield return new WaitForEndOfFrame();
		}
		if (spriteTransform.gameObject != null)
		{
			spriteTransform.localScale = Vector3.one;
			spriteTransform.localPosition = Vector3.zero;
		}
	}

	[ClientRpc]
	void RpcFailedRotation(int rotationAmount)
	{
		if (_shapeSprite)
		{
			StartCoroutine(AnimateFailedRotate(rotationAmount));
		}
	}

	[Client]
	IEnumerator AnimateFailedRotate(int rotationAmount)
	{
		_isRotating = true;
		var spriteTransform = _shapeSprite.transform;
		var timer = 0f;
		while (timer < 1 && spriteTransform.gameObject != null)
		{
			timer += Time.smoothDeltaTime * 15 * (90f / Mathf.Abs(rotationAmount));
			spriteTransform.localEulerAngles = new Vector3(0, 0, Mathf.LerpAngle(0, rotationAmount, timer));
			yield return new WaitForEndOfFrame();
		}
		timer = 0f;
		while (timer < 1 && spriteTransform.gameObject != null)
		{
			timer += Time.smoothDeltaTime * 15 * (90f / Mathf.Abs(rotationAmount));
			spriteTransform.localEulerAngles = new Vector3(0, 0, Mathf.LerpAngle(rotationAmount, 0, timer));
			yield return new WaitForEndOfFrame();
		}
		if (spriteTransform.gameObject != null)
		{
			spriteTransform.localEulerAngles = Vector3.zero;
		}
		_isRotating = false;
	}

	[ClientRpc]
	void RpcSmoothRotation(float startAngle, float endAngle)
	{
		if (_shapeSprite)
		{
			StartCoroutine(SmoothLocalRotation(startAngle, endAngle));
		}
	}

	[Client]
	IEnumerator SmoothLocalRotation(float startAngle, float endAngle)
	{
		_isRotating = true;
		var spriteTransform = _shapeSprite.transform;
		var timer = 0f;
		while (timer < 1 && spriteTransform.gameObject != null)
		{
			timer += Time.smoothDeltaTime * 15;
			spriteTransform.eulerAngles = new Vector3(0, 0, Mathf.LerpAngle(startAngle, endAngle, timer));
			yield return new WaitForEndOfFrame();
		}
		if (spriteTransform.gameObject != null)
		{
			spriteTransform.eulerAngles = new Vector3(0, 0, endAngle);
		}
		_currentRotation = endAngle;
		_isRotating = false;
	}

	[Command]
	private void CmdUpdatePosition(int input)
	{
		ServerUpdatePosition(input);
	}

	[Server]
	public void ForceLock()
	{
		ServerUpdatePosition(RewiredConsts.Action.Lock);
	}

	[Server]
	private void ServerUpdatePosition(int input)
	{
		if (_rigidbody2D)
		{
			switch (input)
			{
				case RewiredConsts.Action.Jump:
					if (_jumpCount > 0)
					{
						if (_jumpCount == _maxJumpCount)
						{
							if (!_grounded && !ModeSelection.Flappy)
							{
								return;
							}
						}
						_jump = true;
					}
					return;
				case RewiredConsts.Action.Lock:
					var returned = false;
					var leftCollide = Physics2D.OverlapAreaAll(new Vector2(-30 - 2f, _startPos.y), new Vector2(-30 + 2f, _startPos.y + 16f));
					foreach (var co in leftCollide)
					{
						if (!returned)
						{
							if (co.gameObject == _rigidbody2D.gameObject)
							{
								_logic.ReturnSupply(_rigidbody2D.name);
								returned = true;
							}
						}
					}
					if (!returned)
					{
						var rightCollide = Physics2D.OverlapAreaAll(new Vector2(30 - 2f, _startPos.y), new Vector2(30 + 2f, _startPos.y + 16f));
						foreach (var co in rightCollide)
						{
							if (!returned)
							{
								if (co.gameObject == _rigidbody2D.gameObject)
								{
									_logic.ReturnSupply(_rigidbody2D.name);
									returned = true;
								}
							}
						}
					}
					_rigidbody2D.GetComponent<Collider2D>().density = 2;
					_rigidbody2D.gameObject.layer = LayerMask.NameToLayer("Block");
					transform.DetachChildren();
					_rigidbody2D.constraints = RigidbodyConstraints2D.None;
					_rigidbody2D.GetComponent<BlockData>().Lock();
					var colorSprites = _rigidbody2D.GetComponentsInChildren<SpriteRenderer>().Where(s => s.color == _rigidbody2D.GetComponent<BlockData>().UnlockedColor).ToList();
					foreach (var s in colorSprites)
					{
						s.color = Color.white;
					}
					if (returned)
					{
						NetworkServer.UnSpawn(_rigidbody2D.gameObject);
						Destroy(_rigidbody2D.gameObject);
					}
					else if (PlatformSelection.ConnectionType == ConnectionType.Server)
					{
						_logic.BlockLocked(_sessionPlayerID);
					}
					foreach (var groundCheck in _rigidbody2D.GetComponentsInChildren<BoxCollider2D>())
					{
						if (groundCheck.gameObject.layer == LayerMask.NameToLayer("GroundCheck"))
						{
							groundCheck.gameObject.layer = LayerMask.NameToLayer("LockGap");
						}
					}
					_shapeSprite.transform.localEulerAngles = Vector3.zero;
					_rigidbody2D = null;
					_shapeSprite = null;
					_moving = false;
					_selecting = true;
					_groundCheck = null;
					_rotateDir = 0;
					_moveDir = 0;
					_jump = false;
					_sfxManager.RpcLock();
					_logic.UpdateShownPointers(_id, LocalPlayerList.PlayerSelection[_id], true);
					RpcResetClientObject();
					return;
				case RewiredConsts.Action.Rotate_Left:
					_rotateDir--;
					return;
				case RewiredConsts.Action.Rotate_Right:
					_rotateDir++;
					return;
			}
		}
	}

	[ClientRpc]
	private void RpcResetClientObject()
	{
		if (_shapeSprite != null && _shapeSprite.gameObject != null)
		{
			_previousSpriteTransform = _shapeSprite.transform;
		}
		_shapeSprite = null;
		_rigidbody2D = null;
	}

	[Client]
	private void FixedUpdateClient()
	{
		var currentBlocks = new List<BlockData>(_knownBlocks);
		foreach (var block in currentBlocks)
		{
			if (block == null || block.gameObject == null)
			{
				_knownBlocks.Remove(block);
			}
			else if (!_knownBlocks.Contains(block))
			{
				_knownBlocks.Add(block);
			}
			else if (block.Locked)
			{
				var blockSprites = block.GetComponentsInChildren<SpriteRenderer>();
				var colorSprites = blockSprites.Where(s => block.Flashing ? s.color == block.UnlockedColor : s.color == Color.white || s.color == block.UnlockedColor).ToList();
				for (var i = 0; i < colorSprites.Count; i++)
				{
					var newColor = block.Flashing ? Color.white : block.LockedColor[i];
					colorSprites[i].color = newColor;
				}
				
				if (!block.Flashing)
				{
					_knownBlocks.Remove(block);
					if (block.LockedColor.All(c => blockSprites.Any(s => s.color == c)))
					{
						Destroy(block);
					}
				}
			}
		}
		if (!_isRotating)
		{
			if (_shapeSprite)
			{
				if (!Mathf.Approximately(_shapeSprite.transform.eulerAngles.z, _currentRotation))
				{
					_shapeSprite.transform.eulerAngles = new Vector3(0, 0, _currentRotation);
				}
			}
			if (_previousSpriteTransform)
			{
				if (_previousSpriteTransform.gameObject == null)
				{
					_previousSpriteTransform = null;
				}
				else
				{
					if (!Mathf.Approximately(_previousSpriteTransform.localEulerAngles.z, 0))
					{
						_previousSpriteTransform.localEulerAngles = Vector3.zero;
					}
					if (!Mathf.Approximately(_previousSpriteTransform.localScale.z, 1))
					{
						_previousSpriteTransform.localScale = Vector3.one;
					}
					if (!Mathf.Approximately(_previousSpriteTransform.localPosition.z, 0))
					{
						_previousSpriteTransform.localPosition = Vector3.zero;
					}
				}
			}
		}
	}

	[ClientRpc]
	private void RpcSyncBlocks()
	{
		foreach (var block in FindObjectsOfType<BlockData>())
		{
			if (block == null || block.gameObject == null)
			{
				_knownBlocks.Remove(block);
			}
			else if (!block.Locked)
			{
				if (!_knownBlocks.Contains(block))
				{
					block.GetComponentsInChildren<SpriteRenderer>().Where(s => s.color == Color.white).ToList().ForEach(s => s.color = block.UnlockedColor);
					_knownBlocks.Add(block);
				}
			}
			else
			{
				var blockSprites = block.GetComponentsInChildren<SpriteRenderer>();
				var colorSprites = blockSprites.Where(s => block.Flashing ? s.color == block.UnlockedColor : s.color == Color.white || s.color == block.UnlockedColor).ToList();
				for (var i = 0; i < colorSprites.Count; i++)
				{
					var newColor = block.Flashing ? Color.white : block.LockedColor[i];
					colorSprites[i].color = newColor;
				}
				if (!block.Flashing)
				{
					if (block.LockedColor.All(c => blockSprites.Any(s => s.color == c)))
					{
						Destroy(block);
					}
				}
			}
		}
		ResyncLockedBlocks();
	}

	private void OnApplicationFocus(bool focus)
	{
		if (focus && isClient)
		{
			ResyncLockedBlocks();
		}
	}

	[Client]
	private void ResyncLockedBlocks()
	{
		foreach (var block in FindObjectsOfType<NetworkRigidbodySetting>())
		{
			if (!block.GetComponent<BlockData>() && block.transform.Find("Shape Sprite"))
			{
				var sprite = block.transform.Find("Shape Sprite");
				sprite.localScale = Vector3.one;
				sprite.localPosition = Vector3.zero;
				sprite.localEulerAngles = Vector3.zero;
			}
		}
	}

	public void CreateShape()
	{
		CmdCreateShape();
	}

	public void UpdatePosition(int input)
	{
		CmdUpdatePosition(input);
	}

	public void UpdatePosition(float input)
	{
		if (!ModeSelection.Flappy)
		{
			if (Mathf.Abs(input) < 0.6f)
			{
				input = 0;
			}
			if (Mathf.Abs(_moveDir - input) >= 0.05f)
			{
				_moveDir = input;
				CmdUpdatePositionFloat(input);
			}
		}
	}

	public void UpdateSelected(int change)
	{
		CmdUpdateSelected(change);
	}

	public void LockSelection()
	{
		CmdLockSelection();
	}

	public void UnlockSelection()
	{
		CmdUnlockSelection();
	}

	public void UpdateSpawnSide(int change)
	{
		CmdUpdateSpawnSide(change);
	}

	public void SetSelected(int selected)
	{
		CmdSetSelected(selected);
	}

	public void SetSpawnSide(int selected)
	{
		CmdSetSpawnSide(selected);
	}

	[Command]
	public void CmdCreateShape()
	{
		ServerCreateShape();
	}

	[Command]
	private void CmdUpdateSelected(int change)
	{
		if (_rigidbody2D)
		{
			return;
		}
		_logic.UpdateSelected(_id, change);
	}

	[Command]
	private void CmdLockSelection()
	{
		if (_logic.GetShape(LocalPlayerList.PlayerSelection[ID], false) != null)
		{
			_selecting = false;
			_logic.UpdatePlayersSelecting(_id, !_selecting);
			_sfxManager.RpcSelect();
		}
	}

	[Command]
	private void CmdUnlockSelection()
	{
		_selecting = true;
		_logic.UpdatePlayersSelecting(_id, !_selecting);
	}

	[Command]
	private void CmdUpdateSpawnSide(int change)
	{
		_logic.UpdateSpawnSide(_id, change);
	}

	[Command]
	private void CmdSetSelected(int selected)
	{
		if (_rigidbody2D)
		{
			return;
		}
		_logic.SetSelected(_id, selected);
	}

	[Command]
	private void CmdSetSpawnSide(int selected)
	{
		if (_rigidbody2D)
		{
			return;
		}
		_logic.SetSpawnSide(_id, selected);
		ServerCreateShape();
	}

	[Command]
	private void CmdUpdatePositionFloat(float input)
	{
		if (_rigidbody2D)
		{
			_moveDir = input;
		}
	}
}
