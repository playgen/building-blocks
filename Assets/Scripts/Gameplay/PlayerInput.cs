﻿using System.Linq;

using Rewired;
using Rewired.ComponentControls;
using Rewired.Integration.UnityUI;

using UnityEngine;

public class PlayerInput : MonoBehaviour
{
	private bool _wasPaused;
	private PlayerController _player;
	private GameUI _gameUI;
	private Player _input;
	[SerializeField]
	private CanvasGroup _mobilePlayingUI;
	[SerializeField]
	private CanvasGroup _mobileSelectingUI;

	private void Start()
	{
		_player = GetComponent<PlayerController>();
		_gameUI = FindObjectOfType<GameUI>();
		_mobilePlayingUI.transform.parent.gameObject.SetActive(Application.isMobilePlatform);
		_mobilePlayingUI.gameObject.SetActive(Application.isMobilePlatform);
		_mobileSelectingUI.gameObject.SetActive(Application.isMobilePlatform);
		_input = PlatformSelection.ConnectionType == ConnectionType.SelfHosting && !Application.isMobilePlatform ? ReInput.players.GetPlayer(_player.ID) : ReInput.players.GetPlayer(RewiredConsts.Player.Menu_Player);
		_input.AddInputEventDelegate(OnInputUpdate, UpdateLoopType.Update);
		if (Application.isMobilePlatform)
		{
			_gameUI.MobileShapeSelect += MobileShapeSelect;
			_input.controllers.maps.SetMapsEnabled(true, ControllerType.Custom, "Default");
		}
		Invoke("JoinDelay", 0.2f);
	}

	private void OnDisable()
	{
		_input.ClearInputEventDelegates();
	}

	private void Update()
	{
		if (_player)
		{
			if (!_wasPaused && _gameUI.IsPaused)
			{
				_wasPaused = true;
			}
			else if (_wasPaused && !_gameUI.IsPaused)
			{
				_wasPaused = false;
				Invoke("InputDelayGameplay", 0.05f);
				Invoke("InputDelaySelection", 0.05f);
			}

			if (Application.isMobilePlatform)
			{
				if (!_gameUI.IsPaused && _gameUI.IsPlaying)
				{
					if (_player.Moving != _mobilePlayingUI.interactable || _player.Moving != _mobilePlayingUI.gameObject.activeSelf)
					{
						_mobilePlayingUI.GetComponentsInChildren<TouchButton>().ToList().ForEach(b => b.enabled = false);
						_mobilePlayingUI.GetComponentsInChildren<TouchButton>().ToList().ForEach(b => b.enabled = true);
						_mobilePlayingUI.interactable = _player.Moving;
						_mobilePlayingUI.blocksRaycasts = _player.Moving;
						_mobilePlayingUI.alpha = _player.Moving ? 1 : 0;
						_mobilePlayingUI.transform.parent.gameObject.SetActive(true);
						_mobilePlayingUI.gameObject.SetActive(true);
						_mobilePlayingUI.GetComponentsInChildren<TouchButton>().ToList().ForEach(b => b.enabled = false);
						_mobilePlayingUI.GetComponentsInChildren<TouchButton>().ToList().ForEach(b => b.enabled = true);
					}
					if (_player.Moving == _mobileSelectingUI.interactable || _player.Moving == _mobilePlayingUI.gameObject.activeSelf)
					{
						_mobileSelectingUI.GetComponentsInChildren<TouchButton>().ToList().ForEach(b => b.enabled = false);
						_mobileSelectingUI.GetComponentsInChildren<TouchButton>().ToList().ForEach(b => b.enabled = true);
						_mobileSelectingUI.interactable = !_player.Moving;
						_mobileSelectingUI.blocksRaycasts = !_player.Moving;
						_mobileSelectingUI.alpha = _player.Moving ? 0 : 1;
						_mobileSelectingUI.transform.parent.gameObject.SetActive(true);
						_mobileSelectingUI.gameObject.SetActive(true);
						_mobileSelectingUI.GetComponentsInChildren<TouchButton>().ToList().ForEach(b => b.enabled = false);
						_mobileSelectingUI.GetComponentsInChildren<TouchButton>().ToList().ForEach(b => b.enabled = true);
					}
				}
				else if (_gameUI.IsPaused || !_gameUI.IsPlaying)
				{
					if (_mobilePlayingUI.interactable || _player.Moving != _mobilePlayingUI.gameObject.activeSelf)
					{
						_mobilePlayingUI.interactable = false;
						_mobilePlayingUI.blocksRaycasts = false;
						_mobilePlayingUI.alpha = 0;
						_mobilePlayingUI.gameObject.SetActive(false);
						_mobilePlayingUI.transform.parent.gameObject.SetActive(false);
					}
					if (_mobileSelectingUI.interactable || _player.Moving == _mobileSelectingUI.gameObject.activeSelf)
					{
						_mobileSelectingUI.interactable = false;
						_mobileSelectingUI.blocksRaycasts = false;
						_mobileSelectingUI.alpha = 0;
						_mobileSelectingUI.gameObject.SetActive(false);
						_mobileSelectingUI.transform.parent.gameObject.SetActive(false);
					}
				}
			}

			if (_player.Moving && !_gameUI.IsPaused)
			{
				_player.UpdatePosition(_input.GetAxis(RewiredConsts.Action.Move_Horizontal));
			}
		}
	}

	void OnInputUpdate(InputActionEventData data)
	{
		if (_gameUI && _gameUI.IsPlaying)
		{
			if ((ReInput.mapping.GetAction(data.actionId).type == InputActionType.Button && data.GetButtonDown()) ||
				(ReInput.mapping.GetAction(data.actionId).type == InputActionType.Axis && !Mathf.Approximately(data.GetAxis(), 0f)))
			{
				if (!_gameUI.IsPaused)
				{
					if (_player.Moving)
					{
						if (!IsInvoking("InputDelaySelection"))
						{
							switch (data.actionId)
							{
								case RewiredConsts.Action.Rotate_Left:
								case RewiredConsts.Action.Rotate_Right:
								case RewiredConsts.Action.Jump:
								case RewiredConsts.Action.Lock:
									_player.UpdatePosition(data.actionId);
									Invoke("InputDelayGameplay", 0.25f);
									break;
							}
						}
					}
					else
					{
						if (!IsInvoking("InputDelayGameplay"))
						{
							if (_player.Selecting)
							{
								switch (data.actionId)
								{
									case RewiredConsts.Action.Shape_Select:
										_player.LockSelection();
										Invoke("InputDelaySelection", 0.5f);
										break;
									case RewiredConsts.Action.Spawn_Left:
										_player.SetSpawnSide(0);
										Invoke("InputDelaySelection", 0.5f);
										break;
									case RewiredConsts.Action.Spawn_Right:
										_player.SetSpawnSide(1);
										Invoke("InputDelaySelection", 0.5f);
										break;
									case RewiredConsts.Action.Move_Selection:
										if (!IsInvoking("InputDelaySelection"))
										{
											if (_input.GetAxis(RewiredConsts.Action.Move_Selection) > 0.25f)
											{
												_player.UpdateSelected(1);
												Invoke("InputDelaySelection", 0.2f);
											}
											else if (_input.GetAxis(RewiredConsts.Action.Move_Selection) < -0.25f)
											{
												_player.UpdateSelected(-1);
												Invoke("InputDelaySelection", 0.2f);
											}
										}
										break;
								}
							}
							else
							{
								switch (data.actionId)
								{
									case RewiredConsts.Action.Shape_Select:
										_player.CreateShape();
										Invoke("InputDelaySelection", 0.2f);
										break;
									case RewiredConsts.Action.Lock:
										_player.UnlockSelection();
										Invoke("InputDelaySelection", 0.5f);
										break;
									case RewiredConsts.Action.Move_Selection:
										if (!IsInvoking("InputDelaySelection"))
										{
											if (_input.GetAxis(RewiredConsts.Action.Move_Selection) > 0.25f)
											{
												_player.UpdateSpawnSide(1);
												Invoke("InputDelaySelection", 0.2f);
											}
											else if (_input.GetAxis(RewiredConsts.Action.Move_Selection) < -0.25f)
											{
												_player.UpdateSpawnSide(-1);
												Invoke("InputDelaySelection", 0.2f);
											}
										}
										break;
								}
							}
						}
					}
				}
				if (!IsInvoking("JoinDelay") && !_gameUI.IsLocked || (_gameUI.IsLocked && PlatformSelection.ConnectionType == ConnectionType.SelfHosting))
				{
					if (data.actionId == RewiredConsts.Action.Pause)
					{
						if (_gameUI.IsPaused && _player.ID != FindObjectOfType<RewiredStandaloneInputModule>().RewiredPlayerIds[0])
						{
							return;
						}
						FindObjectOfType<RewiredStandaloneInputModule>().RewiredPlayerIds = new[] { _input.id };
						_gameUI.TogglePause();
						_player.UpdatePosition(0f);
					}
				}
				if (PlatformSelection.ConnectionType == ConnectionType.Client && data.actionId == RewiredConsts.Action.Info)
				{
					_gameUI.DisplayInfo();
					_player.UpdatePosition(0f);
				}
				if (PlatformSelection.ConnectionType == ConnectionType.Client && _gameUI.DisplayingControls && data.actionId == RewiredConsts.Action.Menu_Select)
				{
					_gameUI.DisplayInfo();
					_player.UpdatePosition(0f);
				}
			}
		}
	}

	private void MobileShapeSelect(int selected)
	{
		if (Application.isMobilePlatform)
		{
			_player.SetSelected(selected);
		}
	}
}