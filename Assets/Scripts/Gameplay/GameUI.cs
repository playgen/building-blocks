﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using PlayGen.Unity.Utilities.AudioManagement;
using PlayGen.Unity.Utilities.Localization;

using Rewired.Integration.UnityUI;

using TMPro;

using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Rewired;

public class GameUI : NetworkBehaviour {

	[SerializeField]
	private GameObject _blockPrefab;
	[SerializeField]
	private Transform _shapeParent;
	[SerializeField]
	private GameObject _gameOverObj;
	[SerializeField]
	private GameObject _readyOverButton;
	[SerializeField]
	private GameObject _restartOverButton;
	[SerializeField]
	private GameObject _quitOverButton;
	[SerializeField]
	private TextMeshProUGUI _finalHeight;
	[SerializeField]
	private TextMeshProUGUI _timerText;
	[SerializeField]
	private TextMeshProUGUI _countdownText;
	[SerializeField]
	private TextMeshProUGUI _overCountdownText;
	[SerializeField]
	private TextMeshProUGUI _highScoreCountdown;
	[SerializeField]
	private TextMeshProUGUI _restartCountdown;
	[SerializeField]
	private GameObject _pauseMenu;
	[SerializeField]
	private GameObject _pauseButtons;
	[SerializeField]
	private GameObject _pauseRestartButton;
	[SerializeField]
	private GameObject _quitRestartButton;
	[SerializeField]
	private TextMeshProUGUI _pauseTitle;
	[SerializeField]
	private GameObject _pauseOverlay;
	[SerializeField]
	private GameObject _startHelp;
	[SerializeField]
	private List<GameObject> _extraTopElements;
	private bool _startUp;
	private readonly List<GameObject> _shapeSupply = new List<GameObject>();
	private bool _isLocked;

	public event Action<int> MobileShapeSelect = delegate {};

	[SerializeField]
	private GameObject _pointerPrefab;
	private readonly List<GameObject> _selectionAreas = new List<GameObject>();
	private readonly Dictionary<int, GameObject> _selectionPointers = new Dictionary<int, GameObject>();

	[SerializeField]
	private List<GameObject> _spawnLeft;
	private readonly List<int> _spawnLeftPlayers = new List<int>(8);
	[SerializeField]
	private List<GameObject> _spawnRight;
	private readonly List<int> _spawnRightPlayers = new List<int>(8);

	public bool IsPaused
	{
		get { return _pauseMenu.activeSelf || _startHelp.activeSelf || _isLocked; }
	}

	public bool IsLocked
	{
		get { return _isLocked || (PlatformSelection.ConnectionType == ConnectionType.SelfHosting && IsPaused); }
	}

	public bool IsPlaying
	{
		get { return !_gameOverObj.activeSelf; }
	}

	public bool DisplayingControls
	{
		get { return _startHelp.activeSelf; }
	}

	public void Draw(GameObject[] shapes, int[] supply, int[] ids, int[] leftSide, int[] rightSide, Dictionary<int, bool> selecting)
	{
		if (!_startUp)
		{
			_gameOverObj.SetActive(false);
			_pauseMenu.SetActive(false);
			_pauseOverlay.SetActive(false);
			if (PlatformSelection.ClientConfig.DisplayControlsAtStart)
			{
				_startHelp.SetActive(true);
				_shapeParent.gameObject.SetActive(false);
			}
			_startUp = true;
		}
		_shapeSupply.Clear();
		_selectionAreas.Clear();
		foreach (Transform t in _shapeParent.transform)
		{
			Destroy(t.gameObject);
		}
		foreach (var point in _selectionPointers)
		{
			Destroy(point.Value);
		}
		_spawnLeftPlayers.Clear();
		_spawnRightPlayers.Clear();
		_selectionPointers.Clear();
		for (var i = 0; i < shapes.Length; i++)
		{
			var newBlock = Instantiate(_blockPrefab, _shapeParent.transform, false);
			var rend = shapes[i].transform.Find("Shape Sprite").GetComponent<SpriteRenderer>();

			newBlock.GetComponentInChildren<Image>().sprite = rend.sprite;
			newBlock.GetComponentInChildren<Image>().transform.localScale = new Vector3(rend.flipX ? -1 : 1, rend.flipY ? -1 : 1, 1);
			newBlock.transform.Find("Amount").GetComponent<TextMeshProUGUI>().text = supply[i].ToString();
			newBlock.GetComponentInChildren<Button>().enabled = Application.isMobilePlatform;
			if (Application.isMobilePlatform)
			{
				newBlock.GetComponentInChildren<Button>().onClick.RemoveAllListeners();
				var thisShape = i;
				newBlock.GetComponentInChildren<Button>().onClick.AddListener(delegate { MobileShapeSelect(thisShape); });
			}
			_shapeSupply.Add(newBlock);
			_selectionAreas.Add(newBlock.transform.Find("Block Pointers").gameObject);
		}
		if (PlatformSelection.ConnectionType == ConnectionType.Client)
		{
			foreach (var uiElement in _extraTopElements)
			{
				Instantiate(uiElement, _shapeParent.transform, false);
			}
		}
		foreach (var player in ids)
		{
			var newPointer = Instantiate(_pointerPrefab, _selectionAreas[0].transform, false);
			newPointer.GetComponent<Image>().color = FindObjectOfType<SharedList>().Colors[player];
			newPointer.GetComponentInChildren<TextMeshProUGUI>().text = (player + 1).ToString();
			_selectionPointers.Add(player, newPointer);
		}
		foreach (var go in _spawnLeft)
		{
			go.GetComponent<SpriteRenderer>().color = Color.white * new Color(1, 1, 1, 0.25f);
			_spawnLeftPlayers.Add(8);
		}
		foreach (var go in _spawnRight)
		{
			go.GetComponent<SpriteRenderer>().color = Color.white * new Color(1, 1, 1, 0.25f);
			_spawnRightPlayers.Add(8);
		}
		for (var i = 0; i < leftSide.Length; i++)
		{
			_spawnLeft[i].GetComponent<SpriteRenderer>().color = FindObjectOfType<SharedList>().Colors[leftSide[i]];
			if (selecting[leftSide[i]])
			{
				_spawnLeft[i].GetComponent<SpriteRenderer>().color *= new Color(1, 1, 1, 0.25f);
			}
			_spawnLeftPlayers[i] = leftSide[i];
		}
		for (var i = 0; i < rightSide.Length; i++)
		{
			_spawnRight[i].GetComponent<SpriteRenderer>().color = FindObjectOfType<SharedList>().Colors[rightSide[i]];
			if (selecting[rightSide[i]])
			{
				_spawnRight[i].GetComponent<SpriteRenderer>().color *= new Color(1, 1, 1, 0.25f);
			}
			_spawnRightPlayers[i] = rightSide[i];
		}
	}

	public void DisplayInfo()
	{
		if (_pauseMenu.activeSelf || !IsPlaying)
		{
			return;
		}
		_startHelp.SetActive(!_startHelp.activeSelf);
		_shapeParent.gameObject.SetActive(!_startHelp.activeSelf);
	}

	public void CloseHelp()
	{
		_startHelp.SetActive(false);
		_shapeParent.gameObject.SetActive(true);
		if (_isLocked)
		{
			_pauseOverlay.SetActive(true);
		}
	}

	public void UpdateSelected(int playerId, int selected)
	{
		_selectionPointers.Where(p => p.Key == playerId).ToList().ForEach(pointer => pointer.Value.transform.SetParent(_selectionAreas[selected].transform, false));
	}

	public void UpdateSpawnSide(Dictionary<int, int> spawnSides, Dictionary<int, bool> selecting)
	{
		var leftSide = spawnSides.Where(p => p.Value == 0).Select(p => p.Key).ToArray();
		var rightSide = spawnSides.Where(p => p.Value == 1).Select(p => p.Key).ToArray();
		for (var i = 0; i < _spawnLeft.Count; i++)
		{
			_spawnLeft[i].GetComponent<SpriteRenderer>().color = Color.white * new Color(1, 1, 1, 0.25f);
			_spawnLeftPlayers[i] = 8;
		}
		for (var i = 0; i < _spawnRight.Count; i++)
		{
			_spawnRight[i].GetComponent<SpriteRenderer>().color = Color.white * new Color(1, 1, 1, 0.25f);
			_spawnRightPlayers[i] = 8;
		}
		for (var i = 0; i < leftSide.Length; i++)
		{
			_spawnLeft[i].GetComponent<SpriteRenderer>().color = FindObjectOfType<SharedList>().Colors[leftSide[i]];
			if (selecting[leftSide[i]])
			{
				_spawnLeft[i].GetComponent<SpriteRenderer>().color *= new Color(1, 1, 1, 0.25f);
			}
			_spawnLeftPlayers[i] = leftSide[i];
		}
		for (var i = 0; i < rightSide.Length; i++)
		{
			_spawnRight[i].GetComponent<SpriteRenderer>().color = FindObjectOfType<SharedList>().Colors[rightSide[i]];
			if (selecting[rightSide[i]])
			{
				_spawnRight[i].GetComponent<SpriteRenderer>().color *= new Color(1, 1, 1, 0.25f);
			}
			_spawnRightPlayers[i] = rightSide[i];
		}
	}

	public void UpdateShownPointers(int id, int shape, bool show)
	{
		_selectionPointers[id].SetActive(show);
		if (!show)
		{
			StartCoroutine(SelectFlash(id, shape));
		}
	}

	public void UpdatePlayersSelecting(int id, bool selecting)
	{
		if (_spawnLeftPlayers.Any(s => s == id))
		{
			var spawnSide = _spawnLeft[_spawnLeftPlayers.IndexOf(id)].GetComponent<SpriteRenderer>();
			spawnSide.color = FindObjectOfType<SharedList>().Colors[id];
			if (!selecting)
			{
				spawnSide.color = spawnSide.color * new Color(1, 1, 1, 0.25f);
			}
		}
		else if (_spawnRightPlayers.Any(s => s == id))
		{
			var spawnSide = _spawnRight[_spawnRightPlayers.IndexOf(id)].GetComponent<SpriteRenderer>();
			spawnSide.color = FindObjectOfType<SharedList>().Colors[id];
			if (!selecting)
			{
				spawnSide.color = spawnSide.color * new Color(1, 1, 1, 0.25f);
			}
		}
	}

	private IEnumerator SelectFlash(int id, int shape)
	{
		var image = _shapeSupply[shape];
		var timer = 0f;
		var previous = image.GetComponentInChildren<Image>().color;
		while (timer < 1)
		{
			image.GetComponentInChildren<Image>().color = FindObjectOfType<SharedList>().Colors[id];
			timer += Time.smoothDeltaTime * 3;
			yield return new WaitForEndOfFrame();
		}
		image.GetComponentInChildren<Image>().color = previous;
		image.GetComponentInChildren<Button>().enabled = Application.isMobilePlatform;
	}

	public void UpdateDraw (int[] supply) {
		for (var i = 0; i < supply.Length; i++)
		{
			_shapeSupply[i].transform.Find("Amount").GetComponent<TextMeshProUGUI>().text = supply[i].ToString();
			_shapeSupply[i].GetComponentInChildren<Image>().color = supply[i] == 0 ? Color.gray : Color.white;
			_shapeSupply[i].GetComponentInChildren<Button>().enabled = Application.isMobilePlatform;
		}
	}

	public void Restart()
	{
		NetworkManager.singleton.ServerChangeScene("TowerTogether");
	}

	public void TogglePause()
	{
		foreach (Transform child in _pauseMenu.transform)
		{
			child.gameObject.SetActive(false);
		}
		_pauseButtons.SetActive(true);
		var paused = IsPaused;
		_pauseMenu.SetActive(!paused);
		_shapeParent.gameObject.SetActive(paused);
		_pauseRestartButton.SetActive(PlatformSelection.ClientConfig.AllowRestarting);
		_quitRestartButton.SetActive(PlatformSelection.ClientConfig.AllowQuitting);
		_startHelp.SetActive(false);
		if (PlatformSelection.ConnectionType == ConnectionType.SelfHosting)
		{
			_pauseTitle.gameObject.SetActive(!paused);
			var playerId = FindObjectOfType<RewiredStandaloneInputModule>().RewiredPlayerIds[0];
			if (playerId < FindObjectOfType<SharedList>().Colors.Length)
			{
				_pauseTitle.color = FindObjectOfType<SharedList>().Colors[FindObjectOfType<RewiredStandaloneInputModule>().RewiredPlayerIds[0]];
			}
		}
		else
		{
			_pauseTitle.gameObject.SetActive(false);
		}
	}

	public void QuitToMenu()
	{
		if (PlatformSelection.ConnectionType == ConnectionType.Client)
		{
			NetworkManager.singleton.StopClient();
		}
		else
		{
			NetworkManager.singleton.ServerChangeScene(NetworkManager.singleton.onlineScene);
		}
	}

	public void Pause()
	{
		_isLocked = true;
		_pauseOverlay.SetActive(true);
	}

	public void Unpause()
	{
		_isLocked = false;
		_pauseOverlay.SetActive(false);
	}

	[ClientRpc]
	public void RpcPause()
	{
		Pause();
	}

	[ClientRpc]
	public void RpcUnpause()
	{
		Unpause();
	}

	[ClientRpc]
	public void RpcTimer(float timer)
	{
		var span = TimeSpan.FromSeconds(Mathf.FloorToInt(timer));
		_timerText.text = timer < 0 ? string.Empty : string.Format("{0:D2}:{1:D2}", (int)span.TotalMinutes, span.Seconds); 
	}

	[ClientRpc]
	public void RpcCountdown(float timer)
	{
		_countdownText.text = Mathf.Approximately(timer, 3) || timer < 0 ? string.Empty : Mathf.CeilToInt(timer).ToString();
		if (!string.IsNullOrEmpty(_countdownText.text))
		{
			SfxManager.Play("Countdown" + Mathf.CeilToInt(timer) + "True");
		}
	}

	[ClientRpc]
	public void RpcMilestone()
	{
		SfxManager.Play("Countdown" + 0 + "True");
	}

	[ClientRpc]
	public void RpcGameOverCountdown(float timer)
	{
		_overCountdownText.text = Mathf.Approximately(timer, 3) || timer < 0 ? string.Empty : Mathf.CeilToInt(timer).ToString();
		if (!string.IsNullOrEmpty(_overCountdownText.text))
		{
			SfxManager.Play("Countdown" + Mathf.CeilToInt(timer) + "False");
		}
	}

	[ClientRpc]
	public void RpcGameOver(float finalHeight, bool startOnReady, float highScore)
	{
		if (IsPlaying)
		{
			SfxManager.Play("Countdown" + 0 + "False");
		}
		_pauseMenu.SetActive(false);
		_pauseOverlay.SetActive(false);
		_shapeParent.gameObject.SetActive(false);
		_startHelp.SetActive(false);
		_gameOverObj.SetActive(true);
		_finalHeight.text = Localization.GetAndFormat("GAME_OVER_FINAL_HEIGHT", false, (Mathf.CeilToInt(finalHeight*100)*0.01f).ToString(Localization.SpecificSelectedLanguage));
		if (PlatformSelection.ConnectionType == ConnectionType.SelfHosting)
		{
			var menuPlayer = ReInput.players.AllPlayers.OrderBy(p => p.id).First(p => p.controllers.hasKeyboard || p.controllers.Controllers.Any()) ?? ReInput.players.GetPlayer(RewiredConsts.Player.Menu_Player);
			FindObjectOfType<RewiredStandaloneInputModule>().RewiredPlayerIds = new[] { menuPlayer.id };
		}
		_highScoreCountdown.text = Localization.GetAndFormat("GAME_OVER_RECORD_HEIGHT_MOBILE", false, (Mathf.CeilToInt(highScore * 100) * 0.01f).ToString(Localization.SpecificSelectedLanguage));
		_readyOverButton.SetActive(startOnReady);
		_restartOverButton.SetActive(PlatformSelection.ClientConfig.AllowRestarting);
		_quitOverButton.SetActive(PlatformSelection.ClientConfig.AllowQuitting);
	}

	[ClientRpc]
	public void RpcRestartCountdown(float timer)
	{
		_restartCountdown.text = Localization.GetAndFormat("GAME_OVER_RESTART_TIMER", false, Mathf.CeilToInt(timer));
	}
}