﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

using PlayGen.Orchestrator.Common;
using PlayGen.Orchestrator.PSL.Common.LRS;
using PlayGen.Unity.Utilities.Localization;

using Rewired;
using Rewired.Integration.UnityUI;

using UnityEngine;
using UnityEngine.Networking;

public class GameLogicControl : NetworkBehaviour
{
	[SerializeField]
	private GameUI _gameUI;
	[SerializeField]
	private GameObject _playerFloor;
	[SerializeField]
	private BoxCollider2D _milestoneMark;
	[SerializeField]
	private PlayerController _playerPrefab;
	[SerializeField]
	private GameObject _seesawPrefab;
	[SerializeField]
	private int _baseCheckpoint;
	private int _currentCheckpoint;
	private static float _currentTime;
	private float _killTimer = 10;
	private float _milestoneHeight;
	private static float _highScore;

	private GameObject[] _shapes;
	[SerializeField]
	private Color[] _playerColors;

	private GameObject _seesaw;
	private List<PlayerController> _players = new List<PlayerController>();
	private List<NetworkConnection> _currentConnections = new List<NetworkConnection>();
	private bool _playerCheck;
	private bool _redrawUI;
	private bool _localPlayerUpdate;
	private CameraAspectSet _cameraAspect;

	private readonly SyncListInt _shapeSupply = new SyncListInt();
	private Dictionary<string, int> _lockedBlocks = new Dictionary<string, int>();
	[SyncVar]
	private float _milestoneTimer;
	[SyncVar]
	private float _gameOverTimer;
	[SyncVar]
	private bool _milestoneMoving;
	[SyncVar]
	private bool _playing;

	[SyncVar]
	private float _restartTimer = 10;

	private bool _localLock;
	private bool _forceStop;

	private void Start()
	{
		_shapes = FindObjectOfType<SharedList>().Shapes;
		_playerColors = FindObjectOfType<SharedList>().Colors;
	}

	private void Update()
	{
		if (isServer)
		{
			if (Application.isEditor)
			{
				if (Input.GetKeyDown(KeyCode.Insert))
				{
					StartCoroutine(MilestoneMet());
				}
				if (Input.GetKeyDown(KeyCode.Delete))
				{
					Stop();
				}
				if (Input.GetKeyDown(KeyCode.PageDown))
				{
					StartCoroutine(ShakeFloor(10));
				}
			}
			if (!_playing && _players.Count == 0)
			{
				if (NetworkServer.connections.Where(c => c != null).All(c => c.isReady))
				{
					StartServer();
				}
			}
			else
			{
				ServerUpdate();
			}
		}
		if (isClient)
		{
			if (!_cameraAspect)
			{
				_cameraAspect = FindObjectOfType<CameraAspectSet>();
			}
		}
	}

	private void OnDisable()
	{
		if (isServer)
		{
			if (PlatformSelection.ServerConfig.ListenForServerMessage)
			{
				PlatformSelection.ServerStateChanged -= StateChange;
			}
		}
	}

	[Server]
	void StartServer()
	{
		_playing = true;
		_milestoneTimer = 3;
		_gameOverTimer = 3;
		_shapes.ToList().ForEach(s => _shapeSupply.Add(0));
		var playerCount = 0;
		LocalPlayerList.PlayerSelection = new Dictionary<int, int>();
		LocalPlayerList.PlayerSide = new Dictionary<int, int>();
		LocalPlayerList.PlayerIsSelecting = new Dictionary<int, bool>();
		if (PlatformSelection.ConnectionType == ConnectionType.Server)
		{
			foreach (var conn in NetworkServer.connections)
			{
				if (conn != null && playerCount < PlatformSelection.ServerMax)
				{
					var player = Instantiate(_playerPrefab);
					_players.Add(player);
					NetworkServer.DestroyPlayersForConnection(conn);
					NetworkServer.AddPlayerForConnection(conn, player.gameObject, 0);
					player.SetID(this, playerCount, _playerColors[playerCount]);
					LocalPlayerList.PlayerSelection.Add(playerCount, 0);
					LocalPlayerList.PlayerSide.Add(playerCount, playerCount % 2);
					LocalPlayerList.PlayerIsSelecting.Add(playerCount, true);
					playerCount++;
				}
			}
		}
		else
		{
			var localPlayers = new List<int>(LocalPlayerList.ConnectedPlayers);
			if (!Application.isMobilePlatform)
			{
				foreach (var local in localPlayers)
				{
					if (!ReInput.players.GetPlayer(local).controllers.Controllers.Any())
					{
						LocalPlayerList.ConnectedPlayers.Remove(local);
					}
				}
				localPlayers = new List<int>(LocalPlayerList.ConnectedPlayers);
			}
			localPlayers.Sort();
			var localConn = NetworkServer.connections.First(c => c != null);
			foreach (var local in localPlayers)
			{
				var player = Instantiate(_playerPrefab);
				_players.Add(player);
				NetworkServer.AddPlayerForConnection(localConn, player.gameObject, (short)(local));
				player.SetID(this, local, _playerColors[local]);
				LocalPlayerList.PlayerSelection.Add(local, 0);
				LocalPlayerList.PlayerSide.Add(local, playerCount % 2);
				LocalPlayerList.PlayerIsSelecting.Add(local, true);
				playerCount++;
			}
		}
		for (var j = 0; j < _shapeSupply.Count; j++)
		{
			switch (ModeSelection.Supply)
			{
				case ModeSelection.SupplyMode.PerBlock:
					_shapeSupply[j] = playerCount == 0 ? 1 : playerCount;
					break;
				case ModeSelection.SupplyMode.Shared:
					_shapeSupply[j] = (playerCount == 0 ? 1 : playerCount) * _shapeSupply.Count;
					break;
			}
		}
		if (ModeSelection.Environment == ModeSelection.EnvironmentMode.Seesaw)
		{
			var seesaw = Instantiate(_seesawPrefab);
			NetworkServer.Spawn(seesaw);
			_seesaw = seesaw;
		}
		_milestoneMark.transform.position = new Vector2(0, _playerFloor.transform.position.y + _baseCheckpoint + (_players.Count * (ModeSelection.Difficulty + (_seesaw ? 1 : 0))) + 0.5f);
		_currentConnections = NetworkServer.connections.Where(c => c != null).ToList();
		var left = LocalPlayerList.PlayerSide.Where(p => p.Value == 0).Select(p => p.Key).ToArray();
		var right = LocalPlayerList.PlayerSide.Where(p => p.Value == 1).Select(p => p.Key).ToArray();
		var selectingKeys = new List<int>();
		var selectingValues = new List<bool>();
		foreach (var pair in LocalPlayerList.PlayerIsSelecting)
		{
			selectingKeys.Add(pair.Key);
			selectingValues.Add(pair.Value);
		}
		RpcStart(_milestoneMark.transform.position.y, LocalPlayerList.PlayerSelection.Keys.ToArray(), left, right, selectingKeys.ToArray(), selectingValues.ToArray());
		if (ModeSelection.MaxTime > 0)
		{
			if (PlatformSelection.ConnectionType == ConnectionType.SelfHosting)
			{
				_currentTime = 0;
			}
			_gameUI.RpcTimer(ModeSelection.MaxTime - _currentTime);
		}
		if (PlatformSelection.ServerConfig.ListenForServerMessage)
		{
			PlatformSelection.ServerStateChanged += StateChange;
			StateChange(PlatformSelection.ServerState);
		}
		if (PlatformSelection.ConnectionType == ConnectionType.SelfHosting)
		{
			_highScore = PlayerPrefs.GetFloat("TallestTowerPlayerCountMode" + ModeSelection.ModeString(false), 0);
		}
		else
		{
			RpcSetLanguage(Localization.SelectedLanguage.Name);
		}
		Invoke("UpdatePlayerList", 1f);
	}

	[Server]
	private void UpdatePlayerList()
	{
		if (_players.All(m => string.IsNullOrEmpty(m.SessionPlayerID) == false))
		{
			PlatformSelection.UpdatePlayers(_players.Select(p => p.SessionPlayerID).ToList());
		}
		else
		{
			Invoke("UpdatePlayerList", 1f);
		}
	}

	[Server]
	void ServerUpdate()
	{
		if (PlatformSelection.ServerConfig.KillServerWhenEmpty)
		{
			if (_killTimer < 0)
			{
				Application.Quit();
			}
			if (_currentConnections.Count == 0)
			{
				_killTimer -= Time.smoothDeltaTime;
			}
			else if (_forceStop)
			{
				_killTimer -= Time.smoothDeltaTime * 0.02f;
			}
			else
			{
				_killTimer = 10;
			}
		}
		if (ModeSelection.MaxTime > 0 && _currentTime > ModeSelection.MaxTime && !_forceStop)
		{
			Stop();
			PlatformSelection.UpdateServerState(GameState.Stopped);
			return;
		}
		if (_gameUI.IsLocked != _localLock)
		{
			var blocks = FindObjectsOfType<Rigidbody2D>();
			foreach (var block in blocks)
			{
				block.simulated = !_gameUI.IsLocked;
			}
			_localLock = _gameUI.IsLocked;
		}
		if (PlatformSelection.ConnectionType == ConnectionType.SelfHosting)
		{
			if (ReInput.players.GetSystemPlayer().GetButtonDown(RewiredConsts.Action.Lobby_Start))
			{
				LocalAssignNextPlayer();
			}
			for (var i = RewiredConsts.Player.Player_1; i <= RewiredConsts.Player.Player_8; i++)
			{
				//TODO Change for keyboard
				if (!LocalPlayerList.ConnectedPlayers.Contains(i) && ReInput.players.GetPlayer(i).GetButtonDown(RewiredConsts.Action.Lobby_Start))
				{
					LocalPlayerList.ConnectedPlayers.Add(i);
					MenuSfxManager.Join();
					_localPlayerUpdate = true;
				}
				//TODO Change for keyboard
				if (LocalPlayerList.ConnectedPlayers.Contains(i) && ReInput.players.GetPlayer(i).GetButtonDown(RewiredConsts.Action.Lobby_Back))
				{
					LocalPlayerList.ConnectedPlayers.Remove(i);
					MenuSfxManager.Leave();
					_localPlayerUpdate = true;
				}
			}
			var localPlayers = new List<int>(LocalPlayerList.ConnectedPlayers);
			foreach (var local in localPlayers)
			{
				if (!ReInput.players.GetPlayer(local).controllers.Controllers.Any())
				{
					LocalPlayerList.ConnectedPlayers.Remove(local);
					MenuSfxManager.Leave();
					_localPlayerUpdate = true;
				}
			}
		}
		if (_localPlayerUpdate)
		{
			_localPlayerUpdate = false;
			var removedIDs = LocalPlayerList.PlayerSelection.Keys.ToList().Where(p => !LocalPlayerList.ConnectedPlayers.Contains(p)).ToList();
			foreach (var player in removedIDs)
			{
				//TODO Add blocks currently being used back into supply
				_players.Where(p => p.ID == player).ToList().ForEach(p => NetworkServer.Destroy(p.gameObject));
				LocalPlayerList.PlayerSelection.Remove(player);
				LocalPlayerList.PlayerSide.Remove(player);
				LocalPlayerList.PlayerIsSelecting.Remove(player);
			}
			_players = _players.Where(p => LocalPlayerList.PlayerSelection.Keys.Contains(p.ID)).ToList();
			var localConn = NetworkServer.connections.First(c => c != null);
			foreach (var local in LocalPlayerList.ConnectedPlayers)
			{
				if (!LocalPlayerList.PlayerSelection.Keys.Contains(local))
				{
					var player = Instantiate(_playerPrefab);
					_players.Add(player);
					NetworkServer.AddPlayerForConnection(localConn, player.gameObject, (short)(local));
					player.SetID(this, local, _playerColors[local]);
					LocalPlayerList.PlayerSelection.Add(local, 0);
					LocalPlayerList.PlayerSide.Add(local, local % 2);
					LocalPlayerList.PlayerIsSelecting.Add(local, true);
				}
			}
			_redrawUI = true;
		}
		if (NetworkServer.connections.Count(c => c != null) != _currentConnections.Count)
		{
			var playerIDs = _players.Where(p => p != null && p.gameObject != null).Select(p => p.ID).ToList();
			var removedIDs = LocalPlayerList.PlayerSelection.Keys.ToList().Where(p => !playerIDs.Contains(p)).ToList();
			foreach (var player in removedIDs)
			{
				LocalPlayerList.PlayerSelection.Remove(player);
				LocalPlayerList.PlayerSide.Remove(player);
				LocalPlayerList.PlayerIsSelecting.Remove(player);
			}
			_players = _players.Where(p => p != null && p.gameObject != null).ToList();
			_currentConnections = NetworkServer.connections.Where(c => c != null).ToList();
			_playerCheck = true;
		}
		if (_redrawUI && _currentConnections.All(c => c.isReady))
		{
			_redrawUI = false;
			var left = LocalPlayerList.PlayerSide.Where(p => p.Value == 0).Select(p => p.Key).ToArray();
			var right = LocalPlayerList.PlayerSide.Where(p => p.Value == 1).Select(p => p.Key).ToArray();
			var selectingKeys = new List<int>();
			var selectingValues = new List<bool>();
			foreach (var pair in LocalPlayerList.PlayerIsSelecting)
			{
				selectingKeys.Add(pair.Key);
				selectingValues.Add(pair.Value);
			}
			RpcStart(_milestoneMark.transform.position.y, LocalPlayerList.PlayerSelection.Keys.ToArray(), left, right, selectingKeys.ToArray(), selectingValues.ToArray());
			RpcUpdateUI();
			foreach (var player in _players)
			{
				RpcUpdateSelected(player.ID, LocalPlayerList.PlayerSelection[player.ID]);
				RpcUpdateShownPointers(player.ID, LocalPlayerList.PlayerSelection[player.ID], !player.Moving);
			}
			if (PlatformSelection.ConnectionType == ConnectionType.Server)
			{
				RpcSetLanguage(Localization.SelectedLanguage.Name);
				if (_playing)
				{
					if (_localLock)
					{
						Pause();
					}
					else
					{
						Unpause();
					}
				}
				else
				{
					_gameUI.RpcGameOver(_milestoneHeight, PlatformSelection.ServerConfig.StartOnPlayersReady, _highScore);
					if (!Mathf.Approximately(_restartTimer, 10))
					{
						_gameUI.RpcRestartCountdown(_restartTimer);
					}
				}
			}
			Invoke("UpdatePlayerList", 1f);
		}
		if (_playerCheck && _currentConnections.All(c => c.isReady))
		{
			_playerCheck = false;
			if (PlatformSelection.ConnectionType == ConnectionType.Server)
			{
				foreach (var conn in _currentConnections.Where(c => c.playerControllers.Count == 0))
				{
					var lowID = 8;
					for (var i = 0; i < 8; i++)
					{
						if (!_players.Select(p => p.ID).Contains(i))
						{
							lowID = i;
							break;
						}
					}
					if (lowID < 8)
					{
						var player = Instantiate(_playerPrefab);
						NetworkServer.AddPlayerForConnection(conn, player.gameObject, 0);
						player.SetID(this, lowID, _playerColors[lowID]);
						LocalPlayerList.PlayerSelection.Add(lowID, 0);
						LocalPlayerList.PlayerSide.Add(lowID, lowID % 2);
						LocalPlayerList.PlayerIsSelecting.Add(lowID, true);
						_players.Add(player);
					}
				}
			}
			_redrawUI = true;
		}
		if (_gameUI.IsLocked || _localLock)
		{
			return;
		}
		_currentTime += Time.smoothDeltaTime;
		if (Mathf.CeilToInt(_currentTime) != Mathf.CeilToInt(_currentTime + Time.smoothDeltaTime))
		{
			_gameUI.RpcTimer(_playing ? ModeSelection.MaxTime - _currentTime : -1);
		}
		if (Mathf.CeilToInt(_currentTime) != Mathf.CeilToInt(_currentTime + Time.smoothDeltaTime))
		{
			if (_playing && ModeSelection.Earthquakes && Mathf.CeilToInt(_currentTime) % 30 == 0)
			{
				StartCoroutine(ShakeFloor(Mathf.CeilToInt(_currentTime) / 30));
			}
		}
		var leftCollide = Physics2D.OverlapAreaAll(new Vector2(-30 - 2f, _playerFloor.transform.position.y), new Vector2(-30 + 2f, _playerFloor.transform.position.y + 16f));
		var sideCollide = leftCollide.Concat(Physics2D.OverlapAreaAll(new Vector2(30 - 2f, _playerFloor.transform.position.y), new Vector2(30 + 2f, _playerFloor.transform.position.y + 16f)));
		foreach (var co in sideCollide)
		{
			if (co.gameObject.layer == LayerMask.NameToLayer("Block"))
			{
				NetworkServer.UnSpawn(co.gameObject);
				Destroy(co.gameObject);
			}
		}
		if (!_milestoneMoving && _playing)
		{
			var milestoneMet = false;
			_milestoneMark.GetOverlappingColliders(Physics2D.DefaultRaycastLayers);
			foreach (var t in CollisionExtensions.Colliders)
			{
				if (t != null)
				{
					if (t.gameObject.layer == LayerMask.NameToLayer("Block"))
					{
						milestoneMet = true;
						break;
					}
				}
			}
			if (milestoneMet && !_forceStop)
			{
				_milestoneTimer -= Time.smoothDeltaTime;
				if (!Mathf.Approximately(_gameOverTimer, 3))
				{
					_gameOverTimer = 3;
					_gameUI.RpcGameOverCountdown(_gameOverTimer);
				}
				if (Mathf.CeilToInt(_milestoneTimer) != Mathf.CeilToInt(_milestoneTimer + Time.smoothDeltaTime) || Mathf.Approximately(_milestoneTimer + Time.smoothDeltaTime, 3))
				{
					_gameUI.RpcCountdown(_milestoneTimer);
				}
				if (_milestoneTimer <= 0)
				{
					_gameUI.RpcMilestone();
					StartCoroutine(MilestoneMet());
					if (ModeSelection.Environment == ModeSelection.EnvironmentMode.Seesaw)
					{
						RpcSeesawMilestoneMet(_seesaw);
					}
				}
			}
			else
			{
				if (!Mathf.Approximately(_milestoneTimer, 3))
				{
					_milestoneTimer = 3;
					_gameUI.RpcCountdown(_milestoneTimer);
				}
			}
			if ((!milestoneMet || _forceStop) && _shapeSupply.Sum() == 0 && _players.All(p => p.Moving == false))
			{
				_gameOverTimer -= Time.smoothDeltaTime;
				if (Mathf.CeilToInt(_gameOverTimer) != Mathf.CeilToInt(_gameOverTimer + Time.smoothDeltaTime) || Mathf.Approximately(_gameOverTimer + Time.smoothDeltaTime, 3))
				{
					_gameUI.RpcGameOverCountdown(_gameOverTimer);
				}
			}
			if (_gameOverTimer < 0)
			{
				if (PlatformSelection.ConnectionType == ConnectionType.SelfHosting)
				{
					_currentTime = ModeSelection.MaxTime + 1;
					_gameUI.RpcTimer(ModeSelection.MaxTime - _currentTime);
				}
				_playing = false;
				_milestoneMark.gameObject.SetActive(false);
				if (PlatformSelection.ServerConfig.StartAfterCountdown || (PlatformSelection.ServerConfig.StartOnServerMessage && _currentTime < ModeSelection.MaxTime - 25))
				{
					StartCoroutine(RestartTimer());
				}
				var highestBlockFound = false;
				_milestoneHeight = _milestoneMark.transform.position.y + 16;
				while (!highestBlockFound)
				{
					if (_milestoneHeight < 0)
					{
						_gameUI.RpcGameOver(0, PlatformSelection.ServerConfig.StartOnPlayersReady, _highScore);
						highestBlockFound = true;
					}
					else
					{
						var rayHit = Physics2D.RaycastAll(new Vector2(-32, _milestoneHeight - 16), Vector2.right, 64);
						if (rayHit.Any(r => r.transform.gameObject.layer == LayerMask.NameToLayer("Block"))) {
							if (_milestoneHeight > _highScore)
							{
								_highScore = _milestoneHeight;
							}
							if (PlatformSelection.ConnectionType == ConnectionType.SelfHosting)
							{
								PlayerPrefs.SetFloat("TallestTowerPlayerCountMode" + ModeSelection.ModeString(false), _highScore);
							}
							_gameUI.RpcGameOver(_milestoneHeight, PlatformSelection.ServerConfig.StartOnPlayersReady, _highScore);
							highestBlockFound = true;
						}
						else
						{
							_milestoneHeight -= 0.01f;
						}
					}
				}
				if (PlatformSelection.ConnectionType == ConnectionType.Server)
				{
					var actualPlayedCount = _lockedBlocks.Count;

					foreach (var player in _players)
					{
						if (!_lockedBlocks.ContainsKey(player.SessionPlayerID))
						{
							_lockedBlocks.Add(player.SessionPlayerID, 0);
						}
					}
					var totalBlocks = _lockedBlocks.Values.Sum();
					var expectedBlocks = Mathf.Round(totalBlocks / (float)actualPlayedCount);
					foreach (var player in _lockedBlocks)
					{
						var teamWorkingRating = actualPlayedCount == 0 ? 0 : Mathf.Clamp01(((1f / actualPlayedCount) - Mathf.Abs((player.Value - expectedBlocks) / totalBlocks)) * actualPlayedCount);
						PlatformSelection.SendLRSData(player.Key, LRSSkillVerb.SolvedAsGroup, teamWorkingRating);
						if (ModeSelection.MaxCheckpoints > 0)
						{
							PlatformSelection.SendLRSData(player.Key, LRSSkillVerb.SetGoal, (float)_currentCheckpoint / ModeSelection.MaxCheckpoints);
						}
						else
						{
							PlatformSelection.SendLRSData(player.Key, LRSSkillVerb.SetGoal, (float)_currentCheckpoint / (6 - ModeSelection.Difficulty));
						}
					}
				}
			}
		}
	}

	[Server]
	public GameObject GetShape(int shape, bool take = true)
	{
		if (_shapes.Length > shape && _shapeSupply[shape] > 0)
		{
			if (take)
			{
				switch (ModeSelection.Supply)
				{
					case ModeSelection.SupplyMode.PerBlock:
						_shapeSupply[shape]--;
						break;
					case ModeSelection.SupplyMode.Shared:
						for (var i = 0; i < _shapeSupply.Count; i++)
						{
							_shapeSupply[i]--;
						}
						break;
				}

				RpcUpdateUI();
			}
			return _shapes[shape];
		}
		return null;
	}

	[Server]
	public void BlockLocked(string playerId)
	{
		if (_lockedBlocks.ContainsKey(playerId))
		{
			_lockedBlocks[playerId]++;
		}
		else
		{
			_lockedBlocks.Add(playerId, 1);
		}
	}

	[Server]
	private IEnumerator ShakeFloor(int level)
	{
		StartCoroutine(_cameraAspect.ScreenShake(level));
		var currentPosition = _playerFloor.transform.position;
		var currentRotation = _playerFloor.transform.eulerAngles;
		var shakeTime = 5f;
		while (shakeTime > 0)
		{
			_playerFloor.transform.position = currentPosition + Random.insideUnitSphere * 0.1f * level;
			_playerFloor.transform.eulerAngles = currentRotation + Random.insideUnitSphere * 0.1f * level;
			shakeTime -= Time.smoothDeltaTime;
			yield return new WaitForEndOfFrame();
		}
		_playerFloor.transform.position = currentPosition;
		_playerFloor.transform.eulerAngles = currentRotation;
	}

	[Server]
	public void UpdateSelected(int playerID, int change)
	{
		var current = LocalPlayerList.PlayerSelection[playerID] + change;
		if (current >= _shapes.Length)
		{
			current = 0;
		}
		else if (current < 0)
		{
			current = _shapes.Length - 1;
		}
		LocalPlayerList.PlayerSelection[playerID] = current;
		RpcUpdateSelected(playerID, current);
	}

	[Server]
	public void UpdateSpawnSide(int playerID, int change)
	{
		var currentSide = LocalPlayerList.PlayerSide[playerID];
		currentSide += change;
		if (currentSide > 1)
		{
			currentSide = 1;
		}
		else if (currentSide < 0)
		{
			currentSide = 0;
		}
		LocalPlayerList.PlayerSide[playerID] = currentSide;
		var keys = new List<int>();
		var values = new List<int>();
		var selectingKeys = new List<int>();
		var selectingValues = new List<bool>();
		foreach (var pair in LocalPlayerList.PlayerSide)
		{
			keys.Add(pair.Key);
			values.Add(pair.Value);
		}
		foreach (var pair in LocalPlayerList.PlayerIsSelecting)
		{
			selectingKeys.Add(pair.Key);
			selectingValues.Add(pair.Value);
		}
		RpcUpdateSpawnSide(keys.ToArray(), values.ToArray(), selectingKeys.ToArray(), selectingValues.ToArray());
	}

	[Server]
	public void ReturnSupply(string shape)
	{
		if (ModeSelection.MaxTime > 0 && _currentTime > ModeSelection.MaxTime)
		{
			return;
		}
		if (ModeSelection.MaxCheckpoints > 0 && _currentCheckpoint >= ModeSelection.MaxCheckpoints)
		{
			return;
		}
		switch (ModeSelection.Supply)
		{
			case ModeSelection.SupplyMode.PerBlock:
				_shapeSupply[_shapes.ToList().FindIndex(s => s.name == shape)]++;
				break;
			case ModeSelection.SupplyMode.Shared:
				for (var i = 0; i < _shapeSupply.Count; i++)
				{
					_shapeSupply[i]++;
				}
				break;
		}
		RpcUpdateUI();
	}

	[Server]
	IEnumerator MilestoneMet()
	{
		//TODO Ensure players leaving or joining during line movement has no affect on new final position of the line
		_currentCheckpoint++;
		RpcMilestoneMet();
		if (ModeSelection.MaxCheckpoints > 0 && _currentCheckpoint >= ModeSelection.MaxCheckpoints)
		{
			ForceStop();
			yield return null;
		}
		else
		{
			_milestoneMoving = true;
			_milestoneTimer = 3;
			for (var i = 0; i < _players.Count; i++)
			{
				for (var j = 0; j < _shapeSupply.Count; j++)
				{
					switch (ModeSelection.Supply)
					{
						case ModeSelection.SupplyMode.PerBlock:
							_shapeSupply[j]++;
							break;
						case ModeSelection.SupplyMode.Shared:
							_shapeSupply[j] += _shapeSupply.Count;
							break;
					}
				}
			}
			RpcUpdateUI();
			var starting = _milestoneMark.transform.position;
			var movePlayerFloor = _milestoneMark.transform.position.y + (_players.Count * (ModeSelection.Difficulty + (_seesaw ? 1 : 0))) - _playerFloor.transform.position.y > 24;
			var timer = 0f;
			var seesawPosition = Vector2.zero;
			var seesawBasePosition = Vector2.zero;
			if (_seesaw)
			{
				_seesaw.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
				seesawPosition = _seesaw.transform.position;
				seesawBasePosition = _seesaw.transform.Find("Base").localPosition;
			}
			while (_milestoneMark.transform.position.y < starting.y + (_players.Count * (ModeSelection.Difficulty + (_seesaw ? 1 : 0))))
			{
				timer += Time.smoothDeltaTime * 0.25f;
				_milestoneMark.transform.position = Vector2.Lerp(starting, new Vector2(starting.x, starting.y + (_players.Count * (ModeSelection.Difficulty + (_seesaw ? 1 : 0)))), timer);
				if (movePlayerFloor)
				{
					if ((_milestoneMark.transform.position.y - _playerFloor.transform.position.y) / 24 > 1)
					{
						RpcUpdateZoom((_milestoneMark.transform.position.y - _playerFloor.transform.position.y) / 24);
					}
				}
				if (_seesaw)
				{
					var seesawBase = _seesaw.transform.Find("Base");
					_seesaw.GetComponent<Rigidbody2D>().MovePosition(Vector2.Lerp(seesawPosition, seesawPosition + new Vector2(0, 1), timer));
					seesawBase.localPosition = Vector2.Lerp(seesawBasePosition, seesawBasePosition + new Vector2(0, -0.5f), timer);
					seesawBase.localScale = Vector3.Lerp(Vector3.one * 2 * -(seesawBasePosition.y - 1f), Vector3.one * 2 * -(seesawBasePosition.y - 1.5f), timer);
				}
				yield return new WaitForEndOfFrame();
			}
			if (_seesaw)
			{
				_seesaw.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePosition;
			}
			_milestoneMoving = false;
		}
	}

	[ClientRpc]
	private void RpcSeesawMilestoneMet(GameObject seesaw)
	{
		StartCoroutine(ClientSeesawMilestoneMet(seesaw));
	}

	[Client]
	private IEnumerator ClientSeesawMilestoneMet(GameObject seesaw)
	{
		var timer = 0f;
		seesaw.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
		var seesawBase = seesaw.transform.Find("Base");
		var seesawBasePosition = seesawBase.localPosition;
		while (timer <= 1)
		{
			timer += Time.smoothDeltaTime * 0.25f;
			seesawBase.localPosition = Vector2.Lerp(seesawBasePosition, (Vector2)seesawBasePosition + new Vector2(0, -0.5f), timer);
			seesawBase.localScale = Vector3.Lerp(Vector3.one * 2 * -(seesawBasePosition.y - 1f), Vector3.one * 2 * -(seesawBasePosition.y - 1.5f), timer);
			yield return new WaitForEndOfFrame();
		}
		seesaw.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePosition;
	}

	[Server]
	IEnumerator RestartTimer()
	{
		while (_restartTimer > 0)
		{
			_restartTimer -= Time.smoothDeltaTime;
			if (Mathf.CeilToInt(_restartTimer) != Mathf.CeilToInt(_restartTimer + Time.smoothDeltaTime) || Mathf.Approximately(_restartTimer + Time.smoothDeltaTime, 10))
			{
				_gameUI.RpcRestartCountdown(_restartTimer);
			}
			yield return new WaitForEndOfFrame();
		}
		NetworkManager.singleton.ServerChangeScene("TowerTogether");
	}

	[Server]
	private void LocalAssignNextPlayer()
	{
		if (LocalPlayerList.ConnectedPlayers.Count >= 8)
		{
			return;
		}
		var rewiredPlayerId = 0;
		for (var i = 0; i <= 8; i++)
		{
			if (i == 8)
			{
				return;
			}
			rewiredPlayerId = i;
			if (!LocalPlayerList.ConnectedPlayers.Contains(i))
			{
				break;
			}
		}
		var rewiredPlayer = ReInput.players.GetPlayer(rewiredPlayerId);

		var systemPlayer = ReInput.players.GetSystemPlayer();
		var inputSources = systemPlayer.GetCurrentInputSources(RewiredConsts.Action.Lobby_Start);

		foreach (var source in inputSources)
		{
			if (source.controllerType == ControllerType.Keyboard)
			{
				LocalAssignKeyboardAndMouseToPlayer(rewiredPlayer, source.controllerMap.layoutId);
				ReInput.players.GetSystemPlayer().controllers.maps.SetMapsEnabled(false, ControllerType.Keyboard, 0, source.controllerMap.layoutId);
				break;
			}
			if (source.controllerType == ControllerType.Joystick)
			{
				var sourceJoystick = source.controller as Joystick;
				if (sourceJoystick != null)
				{
					if (sourceJoystick.systemId.HasValue && LocalPlayerList.AssignedJoysticks.Values.Contains((int)sourceJoystick.systemId.Value))
					{
						var key = LocalPlayerList.AssignedJoysticks.First(j => j.Value == (int)sourceJoystick.systemId.Value).Key;
						rewiredPlayerId = key;
						rewiredPlayer = ReInput.players.GetPlayer(key);
					}
				}
				LocalAssignJoystickToPlayer(rewiredPlayer, source.controller as Joystick);
				break;
			}
			throw new System.NotImplementedException();
		}
		LocalPlayerList.ConnectedPlayers.Add(rewiredPlayerId);
		MenuSfxManager.Join();
		_localPlayerUpdate = true;
		FindObjectOfType<RewiredStandaloneInputModule>().RewiredPlayerIds = new[] { RewiredConsts.Player.Menu_Player };
	}

	[Server]
	private void LocalAssignKeyboardAndMouseToPlayer(Player player, int layoutId)
	{
		player.controllers.maps.SetMapsEnabled(true, ControllerType.Keyboard, 0, layoutId);
		player.controllers.hasKeyboard = true;
		player.controllers.excludeFromControllerAutoAssignment = true;
	}

	[Server]
	private void LocalAssignJoystickToPlayer(Player player, Joystick joystick)
	{
		player.controllers.AddController(joystick, true);
		ReInput.players.GetPlayer(RewiredConsts.Player.Menu_Player).controllers.AddController(joystick, false);
		player.controllers.hasKeyboard = false;
		player.controllers.maps.SetMapsEnabled(true, ControllerType.Joystick, "Default");
		if (!LocalPlayerList.AssignedJoysticks.ContainsKey(player.id))
		{
			LocalPlayerList.AssignedJoysticks.Add(player.id, joystick.id);
		}
	}

	[ClientRpc]
	private void RpcStart(float checkpointY, int[] ids, int[] left, int[] right, int[] selectingKeys, bool[] selectingValues)
	{
		_milestoneMark.transform.position = new Vector2(0, checkpointY);
		_shapes = FindObjectOfType<SharedList>().Shapes;
		_playerColors = FindObjectOfType<SharedList>().Colors;

		var selectingDict = new Dictionary<int, bool>();
		for (var i = 0; i < selectingKeys.Length; i++)
		{
			selectingDict.Add(selectingKeys[i], selectingValues[i]);
		}
		_gameUI.Draw(_shapes, _shapeSupply.ToArray(), ids, left, right, selectingDict);
	}

	[ClientRpc]
	private void RpcSetLanguage(string language)
	{
		Localization.UpdateLanguage(language);
	}

	[ClientRpc]
	private void RpcMilestoneMet()
	{
		_milestoneMark.GetComponentInChildren<ParticleSystem>().Play();
		StartCoroutine(ChangeMilestoneColor());
	}

	[Client]
	private IEnumerator ChangeMilestoneColor()
	{
		var milestoneParticles = _milestoneMark.GetComponentInChildren<ParticleSystem>();
		var milestoneParticlesMain = milestoneParticles.main;
		while (milestoneParticles.isPlaying)
		{
			milestoneParticlesMain.startColor = Random.ColorHSV(0, 1, 0, 1, 0, 1, 1, 1);
			yield return new WaitForEndOfFrame();
		}
	}

	[Server]
	public void SetSelected(int playerID, int selected)
	{
		LocalPlayerList.PlayerSelection[playerID] = selected;
		RpcUpdateSelected(playerID, selected);
	}

	[Server]
	public void SetSpawnSide(int playerID, int selected)
	{
		LocalPlayerList.PlayerSide[playerID] = selected;
		UpdateSpawnSide(playerID, 0);
	}

	[Server]
	public void UpdateShownPointers(int id, int shape, bool show)
	{
		RpcUpdateShownPointers(id, shape, show);
	}

	[Server]
	public void UpdatePlayersSelecting(int id, bool selecting)
	{
		LocalPlayerList.PlayerIsSelecting[id] = !selecting;
		RpcUpdatePlayersSelecting(id, selecting);
	}

	[ClientRpc]
	private void RpcUpdateUI()
	{
		_gameUI.UpdateDraw(_shapeSupply.ToArray());
	}

	[ClientRpc]
	private void RpcUpdateSelected(int playerId, int selected)
	{
		_gameUI.UpdateSelected(playerId, selected);
	}

	[ClientRpc]
	private void RpcUpdateSpawnSide(int[] sideKeys, int[] sideValues, int[] selectingKeys, bool[] selectingValues)
	{
		var sideDict = new Dictionary<int, int>();
		for (var i = 0; i < sideKeys.Length; i++)
		{
			sideDict.Add(sideKeys[i], sideValues[i]);
		}
		var selectingDict = new Dictionary<int, bool>();
		for (var i = 0; i < selectingKeys.Length; i++)
		{
			selectingDict.Add(selectingKeys[i], selectingValues[i]);
		}
		_gameUI.UpdateSpawnSide(sideDict, selectingDict);
	}

	[ClientRpc]
	private void RpcUpdateShownPointers(int id, int shape, bool show)
	{
		_gameUI.UpdateShownPointers(id, shape, show);
	}

	[ClientRpc]
	private void RpcUpdatePlayersSelecting(int id, bool selecting)
	{
		_gameUI.UpdatePlayersSelecting(id, selecting);
	}

	[ClientRpc]
	private void RpcUpdateZoom(float newValue)
	{
		_cameraAspect.UpdateZoom(newValue);
	}

	[Server]
	private void StateChange(GameState state)
	{
		if (state == GameState.Started)
		{
			Unpause();
		}
		if (state == GameState.Paused)
		{
			Pause();
		}
		if (state == GameState.Stopped)
		{
			if (_gameUI.IsPlaying)
			{
				Stop();
			}
		}
	}

	[Server]
	private void Unpause()
	{
		_gameUI.Unpause();
		_gameUI.RpcUnpause();
	}


	[Server]
	private void Pause()
	{
		_gameUI.Pause();
		_gameUI.RpcPause();
	}

	[Server]
	private void Stop()
	{
		Unpause();
		ForceStop();
		_currentTime = ModeSelection.MaxTime + 1;
		_gameUI.RpcTimer(ModeSelection.MaxTime - _currentTime);
		RpcUpdateUI();
	}

	[Server]
	private void ForceStop()
	{
		_forceStop = true;
		foreach (var player in _players)
		{
			player.ForceLock();
		}
		for (var j = 0; j < _shapeSupply.Count; j++)
		{
			_shapeSupply[j] = 0;
		}
	}
}