﻿using System.Collections;

using PlayGen.Unity.Utilities.Loading;

using Rewired;
using Rewired.Integration.UnityUI;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;

public class ClientMenu : MonoBehaviour {

	private static ClientMenu _instance;
	[SerializeField]
	private GameObject _mainMenu;
	[SerializeField]
	private GameObject _gameCreation;
	[SerializeField]
	private GameObject _gameList;
	[SerializeField]
	private GameObject _settings;
	[SerializeField]
	private GameObject _help;
	[SerializeField]
	private GameObject _localLobby;
	[SerializeField]
	private GameObject _localError;

	private bool _clientConfigSetUp;
	private Player _inputPlayer;
	public static Player InputPlayer
	{
		get { return _instance == null ? null : _instance._inputPlayer; }
	}
	private RewiredStandaloneInputModule _inputModule;
	public static RewiredStandaloneInputModule InputModule
	{
		get { return _instance == null ? null : _instance._inputModule; }
	}

	private void Awake()
	{
		_instance = this;
		_inputModule = FindObjectOfType<RewiredStandaloneInputModule>();
		AllOff();
	}

	private void Update()
	{
		if (!_clientConfigSetUp && PlatformSelection.ClientConfig != null)
		{
			_clientConfigSetUp = true;
			if (PlatformSelection.ConnectionType == ConnectionType.SelfHosting)
			{
				if (NetworkManager.singleton.IsClientConnected())
				{
					MainMenu();
					StartCoroutine(MenuFadeIn());
				}
				else
				{
					HostStartError();
				}
			}
			else if (PlatformSelection.ClientConfig.DisplayMainMenuUI)
			{
				MainMenu();
				StartCoroutine(MenuFadeIn());
			}
		}
		if (_inputPlayer == null || _inputPlayer.id != _inputModule.RewiredPlayerIds[0])
		{
			_inputPlayer = ReInput.players.GetPlayer(_inputModule.RewiredPlayerIds[0]);
		}
		EventSystem.current.sendNavigationEvents = !Loading.IsActive;
	}

	private void OnDisable()
	{
		_instance = null;
	}

	public void MainMenu()
	{
		AllOff();
		_mainMenu.SetActive(true);
	}

	public static void ShowMainMenu()
	{
		_instance.MainMenu();
	}

	public void CreateGame()
	{
		AllOff();
		_gameCreation.SetActive(true);
	}

	public void GameList()
	{
		AllOff();
		_gameList.SetActive(true);
	}

	public void Settings()
	{
		AllOff();
		_settings.SetActive(true);
	}

	public void Help()
	{
		AllOff();
		_help.SetActive(true);
	}

	public void LocalLobby()
	{
		AllOff();
		if (Application.isMobilePlatform)
		{
			LocalPlayerList.ConnectedPlayers.Clear();
			LocalPlayerList.ConnectedPlayers.Add(0);
			MenuSfxManager.Join();
			NetworkManager.singleton.ServerChangeScene("TowerTogether");
			return;
		}
		_localLobby.SetActive(true);
	}

	private void HostStartError()
	{
		AllOff();
		_localError.SetActive(true);
	}

	private void AllOff()
	{
		_mainMenu.SetActive(false);
		_gameCreation.SetActive(false);
		_gameList.SetActive(false);
		_settings.SetActive(false);
		_help.SetActive(false);
		_localLobby.SetActive(false);
		_localError.SetActive(false);
	}

	IEnumerator MenuFadeIn()
	{
		var canvasGroup = GetComponent<CanvasGroup>();
		var timer = 0f;
		while (timer < 4)
		{
			timer += Time.smoothDeltaTime;
			canvasGroup.alpha = (timer - 1) / 3;
			yield return new WaitForEndOfFrame();
		}
		canvasGroup.interactable = true;
	}
}
