﻿using System.Collections.Generic;
using System.Linq;

using UnityEngine;

using UnityEngine.UI;

public class ClientMainMenu : MonoBehaviour
{
	[SerializeField]
	private List<GameObject> _localButtons;
	[SerializeField]
	private GameObject _localPlay;
	[SerializeField]
	private GameObject _exitButton;

	private void Start()
	{
		GetComponentsInChildren<Button>(true).ToList().ForEach(b => b.gameObject.SetActive(false));
		if (PlatformSelection.ClientConfig.DisplayMainMenuUI)
		{
			if (PlatformSelection.ConnectionType == ConnectionType.SelfHosting)
			{
				_localButtons.ForEach(b => b.gameObject.SetActive(true));
				GetComponent<EnsureUISelection>().SetDefault(_localPlay);
			}
			else
			{
				GetComponentsInChildren<Button>(true).ToList().ForEach(b => b.gameObject.SetActive(true));
				_localPlay.SetActive(false);
			}
			if (Application.platform != RuntimePlatform.WindowsPlayer)
			{
				_exitButton.SetActive(false);
			}
		}
	}

	private void Update()
	{
		if (ClientMenu.InputPlayer.GetButtonDown(ClientMenu.InputModule.cancelButton))
		{
			Quit();
		}
	}

	public void Quit()
	{
		Application.Quit();
	}
}
