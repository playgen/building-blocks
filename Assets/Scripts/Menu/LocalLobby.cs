﻿using System.Collections.Generic;
using System.Linq;

using PlayGen.Unity.Utilities.Localization;

using Rewired;
using Rewired.Integration.UnityUI;

using RewiredConsts;

using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

using Player = Rewired.Player;

public class LocalLobby : MonoBehaviour {

	[SerializeField]
	private GameObject[] _players;
	private GameObject[] _shapes;
	[SerializeField]
	private GameObject _lobbyUI;
	[SerializeField]
	private GameObject _modeUI;
	[SerializeField]
	private ButtonDirectionalTriggers _environmentLabel;
	[SerializeField]
	private ButtonDirectionalTriggers _supplyLabel;
	[SerializeField]
	private ButtonDirectionalTriggers _timeLabel;
	[SerializeField]
	private Toggle _earthquakeToggle;
	[SerializeField]
	private Toggle _flappyToggle;
	[SerializeField]
	private List<GameObject> _joinPrompt;
	[SerializeField]
	private List<GameObject> _leavePrompt;
	[SerializeField]
	private List<GameObject> _playerOnePrompts;
	[SerializeField]
	private TextMeshProUGUI _currentMode;
	[SerializeField]
	private GameObject _keyboardScheme;
	[SerializeField]
	private GameObject _controllerScheme;
	private int _listCount;
	private static int _timeIndex;

	private void Start()
	{
		InvokeRepeating("UpdatePlayerList", 0, 0.5f);
		SetModeText();
	}

	private void OnEnable()
	{
		ControllerCheck();
		ReInput.ControllerDisconnectedEvent += OnControllerDisconnected;
	}

	private void OnDisable()
	{
		ReInput.ControllerDisconnectedEvent -= OnControllerDisconnected;
	}

	private void Update () {
		if (ClientMenu.InputPlayer.GetButtonDown(RewiredConsts.Action.Lobby_Back))
		{
			if (_lobbyUI.activeSelf)
			{
				ClientMenu.ShowMainMenu();
				MenuSfxManager.Leave();
			}
			else
			{
				ToggleModeSelect();
			}
		}
		if (_lobbyUI.activeSelf)
		{
			if (ClientMenu.InputPlayer.GetButtonDown(Action.Lobby_Mode_Toggle))
			{
				UpdateEnvironmentMode(0);
				UpdateSupplyMode(0);
				UpdateTimeMode(0);
				_earthquakeToggle.isOn = ModeSelection.Earthquakes;
				_flappyToggle.isOn = ModeSelection.Flappy;
				UpdateEarthquakeMode();
				ToggleModeSelect();
				MenuSfxManager.Select();
			}
			else if (!IsInvoking("ModeSwitch") && ClientMenu.InputPlayer.GetButtonDown(RewiredConsts.Action.Lobby_Start) && LocalPlayerList.ConnectedPlayers.Count >= PlatformSelection.ServerConfig.MinPlayersToStart)
			{
				NetworkManager.singleton.ServerChangeScene("TowerTogether");
				MenuSfxManager.Select();
			}
			if (ReInput.players.GetSystemPlayer().GetButtonDown(RewiredConsts.Action.Lobby_Join))
			{
				AssignNextPlayer();
			}
			for (var i = RewiredConsts.Player.Player_1; i <= RewiredConsts.Player.Player_8; i++)
			{
				if (ReInput.players.GetPlayer(i).GetButtonDown(RewiredConsts.Action.Lobby_Join))
				{
					if (!LocalPlayerList.ConnectedPlayers.Contains(i))
					{
						LocalPlayerList.ConnectedPlayers.Add(i);
						DrawPlayerList();
						MenuSfxManager.Join();
					}
				}
				if (ReInput.players.GetPlayer(i).GetButtonDown(RewiredConsts.Action.Lobby_Leave))
				{
					if (LocalPlayerList.ConnectedPlayers.Contains(i))
					{
						LocalPlayerList.ConnectedPlayers.Remove(i);
						DrawPlayerList();
						MenuSfxManager.Leave();
					}
				}
			}
		}
	}

	public void ToggleModeSelect()
	{
		_modeUI.SetActive(!_modeUI.activeSelf);
		_lobbyUI.SetActive(!_lobbyUI.activeSelf);
		Invoke("ModeSwitch", 0.05f);
	}

	public void UpdateEnvironmentMode(int change)
	{
		ModeSelection.UpdateEnvironment(change);
		SetModeText();
		switch (ModeSelection.Environment)
		{
			case ModeSelection.EnvironmentMode.Default:
				_environmentLabel.Label.text = Localization.Get("MODE_ENV_DEFAULT");
				break;
			case ModeSelection.EnvironmentMode.Seesaw:
				_environmentLabel.Label.text = Localization.Get("MODE_ENV_SEESAW");
				break;
		}
		if (change > 0)
		{
			MenuSfxManager.Move();
		}
	}

	public void UpdateSupplyMode(int change)
	{
		ModeSelection.UpdateSupply(change);
		SetModeText();
		switch (ModeSelection.Supply)
		{
			case ModeSelection.SupplyMode.PerBlock:
				_supplyLabel.Label.text = Localization.Get("MODE_SUPPLY_PER_BLOCK");
				break;
			case ModeSelection.SupplyMode.Shared:
				_supplyLabel.Label.text = Localization.Get("MODE_SUPPLY_SHARED");
				break;
		}
		if (change > 0)
		{
			MenuSfxManager.Move();
		}
	}

	public void UpdateTimeMode(int change)
	{
		_timeIndex += change;
		if (_timeIndex < 0)
		{
			_timeIndex = 6;
		}
		if (_timeIndex > 6)
		{
			_timeIndex = 0;
		}
		ModeSelection.SetMaxTimeFromMinutes(_timeIndex == 0? 0 : (7 - _timeIndex) * 5);
		SetModeText();
		switch (_timeIndex)
		{
			case 0:
				_timeLabel.Label.text = Localization.Get("MODE_TIME_UNLIMITED");
				break;
			default:
				_timeLabel.Label.text = Localization.GetAndFormat("MODE_TIME_SET", false, (7 - _timeIndex) * 5);
				break;
		}
		if (change > 0)
		{
			MenuSfxManager.Move();
		}
	}

	public void UpdateEarthquakeMode()
	{
		ModeSelection.UpdateEarthquakes(_earthquakeToggle.isOn);
		SetModeText();
		MenuSfxManager.Move();
	}

	public void UpdateFlappy()
	{
		ModeSelection.UpdateFlappy(_flappyToggle.isOn);
		SetModeText();
		MenuSfxManager.Move();
	}

	private void SetModeText()
	{
		_currentMode.text = Localization.GetAndFormat("LOCAL_LOBBY_SELECTED_MODE", false, ModeSelection.ModeString());
	}

	private void UpdatePlayerList()
	{
		DrawPlayerList();
		_listCount++;
	}

	private void DrawPlayerList()
	{
		if (_shapes == null)
		{
			_shapes = FindObjectOfType<SharedList>().Shapes;
		}
		_joinPrompt.ForEach(p => p.SetActive(true));
		_leavePrompt.ForEach(p => p.SetActive(false));
		_playerOnePrompts.ForEach(p => p.SetActive(false));
		var controllerCount = ReInput.controllers.joystickCount;
		for (var i = 0; i < _players.Length; i++)
		{
			if (LocalPlayerList.ConnectedPlayers.Contains(i))
			{
				_players[i].transform.Find("Joined").gameObject.SetActive(true);
				_players[i].transform.Find("Not Joined").gameObject.SetActive(false);
				var rend = _shapes[(i + _listCount) % _shapes.Length].transform.Find("Shape Sprite").GetComponent<SpriteRenderer>();
				_players[i].transform.Find("Joined/Block").GetComponent<Image>().sprite = rend.sprite;
				_players[i].transform.Find("Joined/Block").GetComponent<Image>().transform.localScale = new Vector3(rend.flipX ? -1 : 1, rend.flipY ? -1 : 1, 1);
				if (ReInput.players.GetPlayer(i).controllers.maps.GetMap(ControllerType.Keyboard, 0, "Default", "Default").enabled)
				{
					_joinPrompt[1].SetActive(false);
					_leavePrompt[1].SetActive(true);
				}
				else if (ReInput.players.GetPlayer(i).controllers.maps.GetMap(ControllerType.Keyboard, 0, "Default", "Default2").enabled)
				{
					_joinPrompt[2].SetActive(false);
					_leavePrompt[2].SetActive(true);
				}
				else if (ReInput.players.GetPlayer(i).controllers.maps.GetMap(ControllerType.Keyboard, 0, "Default", "Default3").enabled)
				{
					_joinPrompt[3].SetActive(false);
					_leavePrompt[3].SetActive(true);
				}
				else
				{
					_leavePrompt[0].SetActive(true);
					controllerCount--;
				}
				if (_players[i].transform.Find("Joined").gameObject.activeSelf)
				{
					var scheme = _players[i].transform.Find("Joined/Scheme").gameObject;
					if (scheme.transform.childCount == 0)
					{
						if (ReInput.players.GetPlayer(i).controllers.joystickCount == 0)
						{
							var schemeUI = Instantiate(_keyboardScheme, scheme.transform);
							var mappings = ReInput.players.GetPlayer(i).controllers.maps;
							schemeUI.transform.Find("TopLeft").GetComponentInChildren<TextMeshProUGUI>().text = mappings.GetFirstButtonMapWithAction(Action.Rotate_Left, true).elementIdentifierName;
							schemeUI.transform.Find("TopMiddle").GetComponentInChildren<TextMeshProUGUI>().text = mappings.GetFirstButtonMapWithAction(Action.Jump, true).elementIdentifierName;
							schemeUI.transform.Find("TopRight").GetComponentInChildren<TextMeshProUGUI>().text = mappings.GetFirstButtonMapWithAction(Action.Rotate_Right, true).elementIdentifierName;
							schemeUI.transform.Find("BottomMiddle").GetComponentInChildren<TextMeshProUGUI>().text = mappings.GetFirstButtonMapWithAction(Action.Lock, true).elementIdentifierName;
							schemeUI.transform.Find("BottomLeft").GetComponentInChildren<TextMeshProUGUI>().text = mappings.ElementMapsWithAction(Action.Move_Horizontal, true).First(m => m.axisContribution == Pole.Negative).elementIdentifierName;
							schemeUI.transform.Find("BottomRight").GetComponentInChildren<TextMeshProUGUI>().text = mappings.ElementMapsWithAction(Action.Move_Horizontal, true).First(m => m.axisContribution == Pole.Positive).elementIdentifierName;
							foreach (var schemeText in schemeUI.GetComponentsInChildren<TextMeshProUGUI>())
							{
								schemeText.text = schemeText.text.Replace("Keypad ", string.Empty);
							}
						}
						else
						{
							var schemeUI = Instantiate(_controllerScheme, scheme.transform);
							schemeUI.transform.Find("TopMiddle").GetComponentInChildren<TextMeshProUGUI>().text = (ReInput.players.GetPlayer(i).controllers.Joysticks.Min(j => j.systemId) + 1).ToString();
							schemeUI.transform.Find("TopMiddle").GetComponentInChildren<TextMeshProUGUI>().color = FindObjectOfType<SharedList>().Colors[i];
						}
					}
				}
			}
			else
			{
				_players[i].transform.Find("Not Joined").gameObject.SetActive(true);
				_players[i].transform.Find("Joined").gameObject.SetActive(false);
			}
		}
		if (controllerCount == 0)
		{
			_joinPrompt[0].SetActive(false);
		}
		var menuPlayer = FindObjectOfType<RewiredStandaloneInputModule>().RewiredPlayerIds[0];
		if ((ReInput.controllers.GetLastActiveController().type == ControllerType.Joystick && ReInput.players.GetPlayer(menuPlayer).controllers.joystickCount > 0) ||
			ReInput.players.GetPlayer(menuPlayer).controllers.maps.GetMaps(ControllerType.Keyboard, 0).All(m => m.enabled == false))
		{
			_playerOnePrompts[0].SetActive(true);
		}
		else
		{
			if (ReInput.players.GetPlayer(menuPlayer).controllers.maps.GetMap(ControllerType.Keyboard, 0, "Default", "Default").enabled)
			{
				_playerOnePrompts[1].SetActive(true);
			}
			else if (ReInput.players.GetPlayer(menuPlayer).controllers.maps.GetMap(ControllerType.Keyboard, 0, "Default", "Default2").enabled)
			{
				_playerOnePrompts[2].SetActive(true);
			}
			else if (ReInput.players.GetPlayer(menuPlayer).controllers.maps.GetMap(ControllerType.Keyboard, 0, "Default", "Default3").enabled)
			{
				_playerOnePrompts[3].SetActive(true);
			}
		}
	}

	private void OnControllerDisconnected(ControllerStatusChangedEventArgs obj)
	{
		ControllerCheck();
	}

	private void ControllerCheck()
	{
		foreach (var player in new List<int>(LocalPlayerList.ConnectedPlayers))
		{
			if (!ReInput.players.GetPlayer(player).controllers.Controllers.Any())
			{
				LocalPlayerList.ConnectedPlayers.Remove(player);
				DrawPlayerList();
				MenuSfxManager.Leave();
			}
		}
		var menuPlayer = ReInput.players.AllPlayers.OrderBy(p => p.id).First(p => p.controllers.hasKeyboard || p.controllers.Controllers.Any()) ?? ReInput.players.GetPlayer(RewiredConsts.Player.Menu_Player);
		FindObjectOfType<RewiredStandaloneInputModule>().RewiredPlayerIds = new[] { menuPlayer.id };
	}

	void AssignNextPlayer()
	{
		if (LocalPlayerList.ConnectedPlayers.Count >= 8)
		{
			return;
		}
		var rewiredPlayerId = 0;
		for (var i = 0; i <= 8; i++)
		{
			if (i == 8)
			{
				return;
			}
			rewiredPlayerId = i;
			if (!LocalPlayerList.ConnectedPlayers.Contains(i) && !LocalPlayerList.AssignedJoysticks.Keys.Contains(i))
			{
				break;
			}
		}
		var rewiredPlayer = ReInput.players.GetPlayer(rewiredPlayerId);

		var systemPlayer = ReInput.players.GetSystemPlayer();
		var inputSources = systemPlayer.GetCurrentInputSources(RewiredConsts.Action.Lobby_Join);

		foreach (var source in inputSources)
		{
			if (source.controllerType == ControllerType.Keyboard)
			{
				AssignKeyboardAndMouseToPlayer(rewiredPlayer, source.controllerMap.layoutId);
				ReInput.players.GetSystemPlayer().controllers.maps.SetMapsEnabled(false, ControllerType.Keyboard, 0, source.controllerMap.layoutId);
				break;
			}
			if (source.controllerType == ControllerType.Joystick)
			{
				var sourceJoystick = source.controller as Joystick;
				if (sourceJoystick != null)
				{
					if (sourceJoystick.systemId.HasValue && LocalPlayerList.AssignedJoysticks.Values.Contains((int)sourceJoystick.systemId.Value))
					{
						var key = LocalPlayerList.AssignedJoysticks.First(j => j.Value == (int)sourceJoystick.systemId.Value).Key;
						rewiredPlayerId = key;
						rewiredPlayer = ReInput.players.GetPlayer(key);
					}
				}
				AssignJoystickToPlayer(rewiredPlayer, source.controller as Joystick);
				break;
			}
			throw new System.NotImplementedException();
		}
		LocalPlayerList.ConnectedPlayers.Add(rewiredPlayerId);
		DrawPlayerList();
		MenuSfxManager.Join();
		var menuPlayer = ReInput.players.AllPlayers.OrderBy(p => p.id).First(p => p.controllers.hasKeyboard || p.controllers.Controllers.Any()) ?? ReInput.players.GetPlayer(RewiredConsts.Player.Menu_Player);
		FindObjectOfType<RewiredStandaloneInputModule>().RewiredPlayerIds = new[] { menuPlayer.id };
	}

	private void AssignKeyboardAndMouseToPlayer(Player player, int layoutId)
	{
		player.controllers.maps.SetMapsEnabled(true, ControllerType.Keyboard, 0, layoutId);
		player.controllers.hasKeyboard = true;
		player.controllers.excludeFromControllerAutoAssignment = true;
	}

	private void AssignJoystickToPlayer(Player player, Joystick joystick)
	{
		player.controllers.AddController(joystick, true);
		if (joystick.id == 0)
		{
			ReInput.players.GetPlayer(RewiredConsts.Player.Menu_Player).controllers.AddController(joystick, false);
		}
		player.controllers.hasKeyboard = false;
		player.controllers.maps.SetMapsEnabled(true, ControllerType.Joystick, "Default");
		if (!LocalPlayerList.AssignedJoysticks.ContainsKey(player.id))
		{
			LocalPlayerList.AssignedJoysticks.Add(player.id, joystick.id);
		}
	}
}
