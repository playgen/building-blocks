﻿using Rewired;
using Rewired.Integration.UnityUI;

using UnityEngine;
using UnityEngine.UI;

public class HelpMenu : MonoBehaviour {

	[SerializeField]
	private Button _backButton;
	[SerializeField]
	private GameObject _controller;
	[SerializeField]
	private GameObject _keyboardOne;
	[SerializeField]
	private GameObject _keyboardTwo;
	[SerializeField]
	private GameObject _keyboardThree;
	[SerializeField]
	private GameObject _keyboardMulti;
	[SerializeField]
	private GameObject _mobile;
	private int _currentActivePlayer;

	private void OnEnable()
	{
		_currentActivePlayer = FindObjectOfType<RewiredStandaloneInputModule>().RewiredPlayerIds[0];
		_controller.SetActive(false);
		_keyboardOne.SetActive(false);
		_keyboardTwo.SetActive(false);
		_keyboardThree.SetActive(false);
		_keyboardMulti.SetActive(false);
		_mobile.SetActive(false);
		if (Application.isMobilePlatform)
		{
			_mobile.SetActive(true);
		}
		else if (ReInput.controllers.joystickCount == 0 || ReInput.players.GetPlayer(_currentActivePlayer).controllers.joystickCount == 0)
		{
			if (PlatformSelection.ConnectionType == ConnectionType.Client)
			{
				if (PlatformSelection.Arrows)
				{
					_keyboardMulti.SetActive(true);
				}
				else
				{
					_keyboardOne.SetActive(true);
				}
				
			}
			else if (ReInput.players.GetPlayer(_currentActivePlayer).controllers.maps.GetMap(ControllerType.Keyboard, 0, "Default", "Default").enabled)
			{
				_keyboardOne.SetActive(true);
			}
			else if (ReInput.players.GetPlayer(_currentActivePlayer).controllers.maps.GetMap(ControllerType.Keyboard, 0, "Default", "Default2").enabled)
			{
				_keyboardTwo.SetActive(true);
			}
			else
			{
				_keyboardThree.SetActive(true);
			}
		}
		else
		{
			_controller.SetActive(true);
		}
	}

	private void Update()
	{
		if (PlatformSelection.ConnectionType == ConnectionType.Client)
		{
			if (PlatformSelection.Arrows && !_keyboardMulti.activeInHierarchy)
			{
				_keyboardMulti.SetActive(true);
				_keyboardOne.SetActive(false);
			}
			else if (!PlatformSelection.Arrows && !_keyboardOne.activeInHierarchy)
			{
				_keyboardMulti.SetActive(false);
				_keyboardOne.SetActive(true);
			}
		}
		if (ClientMenu.InputPlayer != null)
		{
			if (ClientMenu.InputPlayer.GetButtonDown(ClientMenu.InputModule.cancelButton))
			{
				_backButton.onClick.Invoke();
			}
		}
	}
}
