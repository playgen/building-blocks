﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Rendering;

public class FallingBackgroundBlocks : MonoBehaviour {
	[SerializeField]
	private GameObject[] _shapes;
	[SerializeField]
	private GameObject _title;
	private readonly List<GameObject> _objPool = new List<GameObject>();

	private void Start()
	{
		if (SystemInfo.graphicsDeviceType == GraphicsDeviceType.Null || (GetComponent<NetworkIdentity>() && NetworkServer.active && !NetworkClient.active))
		{
			return;
		}
		Instantiate(_title, new Vector3(0, 40, 0), Quaternion.Euler(Vector3.zero));
		InvokeRepeating("MakeRain", 1, Application.isMobilePlatform || Application.platform == RuntimePlatform.WebGLPlayer ? 0.5f : 0.2f);
	}

	private void MakeRain()
	{
		GameObject newShape;
		if (_objPool.Count < (Application.isMobilePlatform || Application.platform == RuntimePlatform.WebGLPlayer ? 100 : 250))
		{
			newShape = Instantiate(_shapes[Random.Range(0, _shapes.Length)]);
		}
		else
		{
			newShape = _objPool[0];
			_objPool.RemoveAt(0);
		}
		newShape.SetActive(false);
		newShape.transform.position = new Vector2(Random.Range(-26, 27), Random.Range(40, 80));
		newShape.transform.eulerAngles = new Vector3(0, 0, Random.Range(0, 4) * 90f);
		_objPool.Add(newShape);
		newShape.GetComponentsInChildren<SpriteRenderer>().Where(s => s.color == Color.white).ToList().ForEach(s => s.color = Random.ColorHSV(0, 0.95f, 0.5f, 0.95f, 0.7f, 0.95f));
		StartCoroutine(TouchCheck(newShape));
	}

	IEnumerator TouchCheck(GameObject newBlock)
	{
		yield return new WaitForFixedUpdate();
		while (newBlock.GetComponent<Rigidbody2D>().IsTouchingLayers())
		{
			newBlock.transform.position = new Vector2(Random.Range(-26, 27), Random.Range(40, 80));
			newBlock.transform.eulerAngles = new Vector3(0, 0, Random.Range(0, 4) * 90f);
			yield return new WaitForFixedUpdate();
		}
		newBlock.SetActive(true);
	}
}
