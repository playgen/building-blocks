﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

using PlayGen.Orchestrator.Common;
using PlayGen.Unity.Utilities.Localization;

using Rewired;
using Rewired.Integration.UnityUI;

using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OnlineMenu : NetworkBehaviour {
	private float _startTimer = 20;
	private bool _startMessageReceived;
	private float _killTimer = 30;

	[SerializeField]
	private GameObject _canvas;
	[SerializeField]
	private GameObject[] _players;
	[SerializeField]
	private OnlineMenuPlayer _playerPrefab;
	private List<OnlineMenuPlayer> _menuPlayers = new List<OnlineMenuPlayer>();
	[SerializeField]
	private TextMeshProUGUI _startTimerText;
	private int _listCount;
	private GameObject[] _shapes;
	private Player _inputPlayer;
	private RewiredStandaloneInputModule _inputModule;

	private void Start() {
		_inputModule = FindObjectOfType<RewiredStandaloneInputModule>();
		_shapes = FindObjectOfType<SharedList>().Shapes;
		if (isServer)
		{
			if (PlatformSelection.ServerConfig.ListenForServerMessage)
			{
				PlatformSelection.ServerStateChanged += StateChange;
			}
			InvokeRepeating("DrawPlayerList", 0, 1f);
		}
		if (isClient)
		{
			StartCoroutine(MenuFadeIn());
		}
	}

	private void StartMessage()
	{
		_startMessageReceived = true;
	}

	private void Update()
	{
		if (isServer)
		{
			var connections = NetworkServer.connections.Where(c => c != null && c.isReady).ToList();
			if (PlatformSelection.ServerConfig.KillServerWhenEmpty)
			{
				if (_killTimer < 0)
				{
					Application.Quit();
				}
				if (!NetworkServer.connections.Any(c => c != null && c.isReady))
				{
					_killTimer -= Time.smoothDeltaTime;
				}
				else
				{
					_killTimer = 10;
				}
			}
			if (PlatformSelection.ServerConfig.StartAfterCountdown)
			{
				_startTimer = connections.Count >= PlatformSelection.ServerConfig.MinPlayersToStart ? _startTimer - Time.smoothDeltaTime : 20;
			}
			if (connections.Count != _menuPlayers.Count)
			{
				_menuPlayers = _menuPlayers.Where(p => p != null && p.gameObject != null).ToList();
				foreach (var conn in connections.Where(c => c.playerControllers.Count == 0))
				{
					var player = Instantiate(_playerPrefab);
					_menuPlayers.Add(player);
					NetworkServer.AddPlayerForConnection(conn, player.gameObject, 0);
				}
				Invoke("UpdatePlayerList", 1f);
			}
			if (connections.Count >= PlatformSelection.ServerConfig.MinPlayersToStart && 
				(!PlatformSelection.ServerConfig.StartOnServerMessage || (PlatformSelection.ServerConfig.StartOnServerMessage && _startMessageReceived)))
			{
				if (PlatformSelection.ServerConfig.StartAfterCountdown && _startTimer > 0)
				{
					return;
				}
				StartGame();
			}
		}
		if (isClient)
		{
			if (_inputPlayer == null || _inputPlayer.id != _inputModule.RewiredPlayerIds[0])
			{
				_inputPlayer = ReInput.players.GetPlayer(_inputModule.RewiredPlayerIds[0]);
			}
			if (_inputPlayer.GetButtonDown(_inputModule.cancelButton))
			{
				Quit();
			}
		}
	}

	private void OnDisable()
	{
		if (PlatformSelection.ServerConfig.ListenForServerMessage)
		{
			PlatformSelection.ServerStateChanged -= StateChange;
		}
	}

	[Server]
	private void UpdatePlayerList()
	{
		if (_menuPlayers.All(m => string.IsNullOrEmpty(m.PlayerID) == false))
		{
			PlatformSelection.UpdatePlayers(_menuPlayers.Select(p => p.PlayerID).ToList());
		}
		else
		{
			Invoke("UpdatePlayerList", 1f);
		}
	}

	[Server]
	private void StateChange(GameState state)
	{
		if (state == GameState.Started)
		{
			StartMessage();
		}
		if (state == GameState.Stopped)
		{
            RpcQuit();
        }
	}

	public void ReadyUp()
	{

	}

    [ClientRpc]
    private void RpcQuit()
    {
        NetworkManager.singleton.StopClient();
    }

	public void Quit()
	{
		NetworkManager.singleton.StopClient();
	}

	[Server]
	private void DrawPlayerList()
	{
		var timerText = string.Empty;
		var playerCount = NetworkServer.connections.Count(c => c != null && c.isReady);
		if (playerCount < PlatformSelection.ServerMax)
		{
			timerText = Localization.Get("ONLINE_LOBBY_WAITING_PLAYERS");
		}
		else if (PlatformSelection.ServerConfig.StartOnPlayersReady)
		{
			timerText = Localization.Get("ONLINE_LOBBY_WAITING_READY");
		}
		else if (PlatformSelection.ServerConfig.StartOnServerMessage && !_startMessageReceived)
		{
			timerText = Localization.Get("ONLINE_LOBBY_WAITING_SIGNAL");
		}
		else if (PlatformSelection.ServerConfig.StartAfterCountdown)
		{
			timerText += Localization.GetAndFormat("ONLINE_LOBBY_WAITING_COUNTDOWN", false, Mathf.FloorToInt(_startTimer));
		}
		RpcDrawPlayerList(playerCount, _menuPlayers.Select(p => p.PlayerName).ToArray(), timerText);
	}

	[Client]
	IEnumerator MenuFadeIn()
	{
		var canvasGroup = _canvas.GetComponent<CanvasGroup>();
		var timer = 0f;
		while (timer < 4)
		{
			timer += Time.smoothDeltaTime;
			canvasGroup.alpha = (timer - 1) / 3;
			yield return new WaitForEndOfFrame();
		}
		canvasGroup.interactable = true;
	}

	[ClientRpc]
	private void RpcDrawPlayerList(int playerCount, string[] playerNames, string waitMessage)
	{
		if (_shapes == null)
		{
			_shapes = FindObjectOfType<SharedList>().Shapes;
		}
		for (var i = 0; i < _players.Length; i++)
		{
			if (i < playerCount)
			{
				_players[i].SetActive(true);
				var rend = _shapes[(i + _listCount) % _shapes.Length].transform.Find("Shape Sprite").GetComponent<SpriteRenderer>();
				_players[i].transform.Find("Block").GetComponent<Image>().sprite = rend.sprite;
				_players[i].transform.Find("Block").GetComponent<Image>().transform.localScale = new Vector3(rend.flipX ? -1 : 1, rend.flipY ? -1 : 1, 1);
				_players[i].GetComponentInChildren<TextMeshProUGUI>().text = i < playerNames.Length ? playerNames[i] : string.Empty;
			}
			else
			{
				_players[i].SetActive(false);
			}
		}
		_startTimerText.text = waitMessage;
		_listCount++;
	}

	private void StartGame()
	{
		foreach (var conn in NetworkServer.connections.Where(c => c != null && c.isReady).Where(c => c.playerControllers.Count != 0))
		{
			NetworkServer.DestroyPlayersForConnection(conn);
		}
		PlatformSelection.UpdateServerState(GameState.Started);
		SceneManager.LoadScene("TowerTogether");
		NetworkManager.singleton.ServerChangeScene("TowerTogether");
	}
}
