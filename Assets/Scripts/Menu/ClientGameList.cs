﻿using PlayGen.Unity.Utilities.Loading;
using UnityEngine;

public class ClientGameList : MonoBehaviour {

	[SerializeField]
	private Transform _gameListParent;
	[SerializeField]
	private GameObject _gamePrefab;

	private void OnEnable()
	{
		InvokeRepeating("LoadGameList", 0, 15);
	}

	private void Update()
	{
		if (ClientMenu.InputPlayer.GetButtonDown(ClientMenu.InputModule.cancelButton))
		{
			ClientMenu.ShowMainMenu();
		}
	}

	public void LoadGameList()
	{
		Loading.Start();
		//TODO: Reimplement
		/*ClientAPI.instance.FindGameRooms(new List<Property>(),
			new List<Property>{
								new Property
									{
										name = "playing",
										value = "true"
									}}, DrawGameList);*/
		Loading.Stop();
	}

	//TODO: Reimplement
	/*private void DrawGameList(SpawnedGameNetwork[] gameList)
	{
		foreach (Transform child in _gameListParent)
		{
			Destroy(child.gameObject);
		}
		foreach (var game in gameList)
		{
			var gameObj = Instantiate(_gamePrefab, _gameListParent);
			gameObj.transform.Find("Name").GetComponent<TextMeshProUGUI>().text = game.name;
			gameObj.transform.Find("Players").GetComponent<TextMeshProUGUI>().text = game.numPlayers + "/" + game.maxPlayers;
			var gameStore = game;
			gameObj.GetComponentInChildren<Button>().onClick.AddListener(delegate { JoinGameServer(gameStore.ip, gameStore.port); });
		}
	}*/

	private void JoinGameServer(string ip, int port)
	{
		Loading.Start();
		//TODO: Reimplement
		//ClientAPI.instance.JoinGameServer(ip, port);
		Loading.Stop();
	}
}
