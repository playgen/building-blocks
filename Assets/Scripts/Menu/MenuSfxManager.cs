﻿using PlayGen.Unity.Utilities.AudioManagement;

using Rewired;
using Rewired.Integration.UnityUI;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuSfxManager : MonoBehaviour
{
	private int _disableDelay;
	private GameObject _lastSelected;
	private RewiredStandaloneInputModule _inputModule;
	private Player _inputPlayer;
	private static MenuSfxManager _instance;

	private void Awake()
	{
		if (_instance)
		{
			Destroy(gameObject);
			return;
		}
		_instance = this;
		DontDestroyOnLoad(this);
	}

	private void OnEnable()
	{
		SceneManager.activeSceneChanged += SceneChange;
	}

	private void OnDisable()
	{
		SceneManager.activeSceneChanged -= SceneChange;
	}

	private void SceneChange(Scene arg0, Scene arg1)
	{
		_inputModule = FindObjectOfType<RewiredStandaloneInputModule>();
	}

	void Update()
	{
		if (_inputPlayer == null || _inputPlayer.id != _inputModule.RewiredPlayerIds[0])
		{
			_inputPlayer = ReInput.players.GetPlayer(_inputModule.RewiredPlayerIds[0]);
		}
		if (_lastSelected == null)
		{
			_lastSelected = null;
		}
		if (_lastSelected != null && _lastSelected.GetComponentInChildren<Selectable>().interactable)
		{
			if (_lastSelected.activeInHierarchy)
			{
				_disableDelay = 2;
			}
			else if (!_lastSelected.activeInHierarchy && _disableDelay > 0)
			{
				_disableDelay--;
			}
			if (_inputPlayer.GetButtonDown(_inputModule.submitButton) && _disableDelay > 0)
			{
				if (_lastSelected.GetComponent<Toggle>() || _lastSelected.GetComponent<Button>() || _lastSelected.GetComponent<Dropdown>())
				{
					Select();
				}
			}
			else if (_inputPlayer.GetButtonDown(_inputModule.cancelButton) && SceneManager.GetActiveScene().name != "TowerTogether")
			{
				Leave();
			}
		}
		if (_lastSelected != EventSystem.current.currentSelectedGameObject)
		{
			if (_lastSelected != null && EventSystem.current.currentSelectedGameObject)
			{
				if (_lastSelected.GetComponentInParent<EnsureUISelection>() == EventSystem.current.currentSelectedGameObject.GetComponentInParent<EnsureUISelection>())
				{
					Move();
				}
			}
			_lastSelected = EventSystem.current.currentSelectedGameObject;
		}
	}

	public static void Move()
	{
		SfxManager.Play("Move");
	}

	public static void Select()
	{
		SfxManager.Play("Select");
	}

	public static void Join()
	{
		SfxManager.Play("Join");
	}

	public static void Leave()
	{
		SfxManager.Play("Leave");
	}
}
