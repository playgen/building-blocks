﻿using UnityEngine;
using UnityEngine.Networking;

public class OnlineMenuPlayer : NetworkBehaviour
{
	[SyncVar]
	private string _name;
	private string _id;
	public string PlayerID
	{
		get { return _id; }
	}
	public string PlayerName
	{
		get { return _name; }
	}

	void Start () {
		if (hasAuthority)
		{
			CmdSetName(PlatformSelection.PlayerDetails.Id.ToString(), PlatformSelection.PlayerDetails.Name);
		}
	}

	private void Update()
	{
		if (isClient)
		{
			if (!ClientScene.ready)
			{
				ClientScene.Ready(NetworkManager.singleton.client.connection);
			}
		}
	}

	[Command]
	private void CmdSetName(string playerID, string playerName)
	{
		_name = playerName;
		_id = playerID;
		Debug.Log(_name);
	}
}