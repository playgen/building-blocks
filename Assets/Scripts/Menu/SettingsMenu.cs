﻿using System.Collections.Generic;
using System.Linq;

using PlayGen.Unity.Utilities.AudioManagement;
using PlayGen.Unity.Utilities.Localization;

using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour {

	[SerializeField]
	private List<GameObject> _standaloneOnly;
	[SerializeField]
	private TMP_Dropdown _resolution;
	[SerializeField]
	private Toggle _fullScreen;
	[SerializeField]
	private Slider _masterVolume;
	[SerializeField]
	private Slider _musicVolume;
	[SerializeField]
	private Slider _soundVolume;
	[SerializeField]
	private TextMeshProUGUI _currentlyPlaying;
	[SerializeField]
	private Button _backButton;

	private void OnEnable()
	{
		_standaloneOnly.ForEach(s => s.SetActive(Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor));
		if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor)
		{
			_resolution.ClearOptions();
			var resolutions = Screen.resolutions.Distinct().ToList();
			resolutions = resolutions.Where(res => res.width >= 960 && res.height >= 540).ToList();
			var current = new Resolution { height = Screen.height, width = Screen.width };
			if (!resolutions.Any(res => res.width == current.width && res.height == current.height))
			{
				resolutions.Add(current);
			}
			var resolutionStrings = resolutions.OrderByDescending(res => res.width).ThenByDescending(res => res.height).Select(res => res.width + " x " + res.height).ToList();
			_resolution.AddOptions(resolutionStrings);
			_resolution.value = resolutionStrings.IndexOf(Screen.width + " x " + Screen.height);
			_fullScreen.isOn = Screen.fullScreen;
			var parentCanvas = GetComponentInParent<Canvas>();
			 _resolution.template.gameObject.AddComponent<Canvas>();
			var templateCanvas = _resolution.template.gameObject.GetComponent<Canvas>();
			if (templateCanvas)
			{
				templateCanvas.overrideSorting = false;
				templateCanvas.sortingOrder = parentCanvas.sortingOrder;
				templateCanvas.sortingLayerName = parentCanvas.sortingLayerName;
			}
		}
		_masterVolume.value = Mathf.Round(PlayerPrefs.GetFloat("Master Volume", 1) * _masterVolume.maxValue);
		_musicVolume.value = Mathf.Round(PlayerPrefs.GetFloat("Music Volume", 1) * _musicVolume.maxValue);
		_soundVolume.value = Mathf.Round(PlayerPrefs.GetFloat("Sound Volume", 1) * _soundVolume.maxValue);
		MusicManager.TrackChange += UpdatePlaying;
		UpdatePlaying();
	}

	private void Update()
	{
		if (ClientMenu.InputPlayer != null)
		{
			if (ClientMenu.InputPlayer.GetButtonDown(ClientMenu.InputModule.cancelButton))
			{
				_backButton.onClick.Invoke();
			}
		}
	}

	private void OnDisable()
	{
		MusicManager.TrackChange -= UpdatePlaying;
	}

	public void Apply()
	{
		if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor)
		{
			var newResolutionSplit = _resolution.options[_resolution.value].text.Split('x');
			var newResolution = new Resolution { width = int.Parse(newResolutionSplit[0]), height = int.Parse(newResolutionSplit[1]) };
			var fullScreen = _fullScreen.isOn;

			Screen.SetResolution(newResolution.width, newResolution.height, fullScreen);
			if (FindObjectOfType<CameraAspectSet>())
			{
				FindObjectOfType<CameraAspectSet>().SetCameraSize();
			}
		}
		PlayerPrefs.SetFloat("Master Volume", _masterVolume.value / _masterVolume.maxValue);
		PlayerPrefs.SetFloat("Sound Volume", _soundVolume.value / _soundVolume.maxValue);
		AudioListener.volume = PlayerPrefs.GetFloat("Master Volume", 1);
		SfxManager.UpdateVolume();
		if (FindObjectOfType<MusicManager>())
		{
			FindObjectOfType<MusicManager>().UpdateVolume(_musicVolume.value / _musicVolume.maxValue);
		}
		FindObjectsOfType<PlayerSfxManager>().ToList().ForEach(p => p.UpdateVolume());
	}

	public void PlayNextTrack()
	{
		MusicManager.NextTrack();
	}

	private void UpdatePlaying()
	{
		_currentlyPlaying.text = Localization.GetAndFormat("SETTINGS_MUSIC_PLAYING", false, MusicManager.CurrentTrack.Artist + " - " + MusicManager.CurrentTrack.TrackName);
	}
}
