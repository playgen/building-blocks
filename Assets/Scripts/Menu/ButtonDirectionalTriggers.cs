﻿using System.Collections;

using Rewired;
using Rewired.Integration.UnityUI;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class ButtonDirectionalTriggers : MonoBehaviour {
	private Player _inputPlayer;
	private RewiredStandaloneInputModule _inputModule;

	[SerializeField]
	private Image _leftButton;
	[SerializeField]
	private Image _rightButton;
	[SerializeField]
	public TextMeshProUGUI Label;

	public UnityEvent Left;
	public UnityEvent Right;

	private bool _selected;

	private void OnEnable()
	{
		_inputModule = FindObjectOfType<RewiredStandaloneInputModule>();
	}

	void Update () {
		if (EventSystem.current.currentSelectedGameObject == gameObject)
		{
			if (!_selected)
			{
				StopAllCoroutines();
				StartCoroutine(Selection(true));
			}
			if (_inputPlayer == null || _inputPlayer.id != _inputModule.RewiredPlayerIds[0])
			{
				_inputPlayer = ReInput.players.GetPlayer(_inputModule.RewiredPlayerIds[0]);
			}
			if (!IsInvoking("ModeChange") && Mathf.Abs(_inputPlayer.GetAxis(RewiredConsts.Action.Move_Horizontal)) > 0.6f)
			{
				Invoke("ModeChange", 0.2f);
				var action = _inputPlayer.GetAxis(RewiredConsts.Action.Move_Horizontal) < 0 ? Left : Right;
				var button = _inputPlayer.GetAxis(RewiredConsts.Action.Move_Horizontal) < 0 ? _leftButton : _rightButton;
				action.Invoke();
				StartCoroutine(ButtonSelection(button));
			}
		}
		else
		{
			if (_selected)
			{
				StopAllCoroutines();
				StartCoroutine(Selection(false));
			}
		}
	}

	private IEnumerator ButtonSelection(Image button)
	{
		var startColor = button.color;
		var newColor = new Color(0, 1, 0, 1);
		var timer = 0f;
		while (timer < 1)
		{
			timer += Time.smoothDeltaTime * 10;
			button.color = Color.Lerp(startColor, newColor, timer);
			yield return new WaitForEndOfFrame();
		}
		timer = 0f;
		startColor = new Color(0, 1, 0, 1);
		newColor = new Color(0, 0.4634213f, 1, 1);
		while (timer < 1)
		{
			timer += Time.smoothDeltaTime * 10;
			button.color = Color.Lerp(startColor, newColor, timer);
			yield return new WaitForEndOfFrame();
		}
	}

	private IEnumerator Selection(bool selected)
	{
		_selected = selected;
		var startColor = Label.color;
		var newColor = selected ? new Color(0, 0.4634213f, 1, 1) : Color.white;
		var timer = 0f;
		Invoke("ModeChange", 0.333f);
		while (timer < 1)
		{
			timer += Time.smoothDeltaTime * 3;
			_leftButton.color = Color.Lerp(startColor, newColor, timer);
			_rightButton.color = Color.Lerp(startColor, newColor, timer);
			Label.color = Color.Lerp(startColor, newColor, timer);
			yield return new WaitForEndOfFrame();
		}
	}
}
