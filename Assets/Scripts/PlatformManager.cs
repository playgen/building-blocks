﻿using System;
using UnityEngine.Networking;

public enum ConnectionType
{
	Client,
	Server,
	SelfHosting
}

[Serializable]
public class PlatformManager
{
	public ConnectionType ConnectionType;
	public NetworkManager NetworkManagerObj;
}