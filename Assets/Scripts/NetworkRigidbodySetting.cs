﻿using UnityEngine;
using UnityEngine.Networking;

public class NetworkRigidbodySetting : NetworkBehaviour {

	void Start () {
		GetComponent<Rigidbody2D>().bodyType = isServer ? RigidbodyType2D.Dynamic : RigidbodyType2D.Kinematic;
		GetComponent<Rigidbody2D>().useFullKinematicContacts = false;
	}
}
