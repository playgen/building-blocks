﻿using System;

[Serializable]
public class PlatformServerConfig {

	public bool ListenForServerMessage;
	public int MinPlayersToStart;
	public bool StartOnPlayersReady;
	public bool StartOnServerMessage;
	public bool StartAfterCountdown;
	public bool KillServerWhenEmpty;
}
