﻿using PlayGen.Unity.Utilities.Localization;
using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEditor;
using UnityEngine;

public class CharacterCheck : EditorWindow
{
    private TextMeshProUGUI _text;

    [MenuItem("Tools/Localization Check")]
    public static void ShowWindow()
    {
        GetWindow(typeof(CharacterCheck), true, "Localization Check", true);
    }

    void OnGUI()
    {
        _text = (TextMeshProUGUI)EditorGUILayout.ObjectField(_text, typeof(TextMeshProUGUI), true);

        if (_text)
        {
            if (GUILayout.Button("Set", GUILayout.ExpandWidth(false)))
            {
                Localization.UpdateLanguage(Localization.SelectedLanguage != null ? Localization.SelectedLanguage.Name : "en");
                var jsonTextAssets = Resources.LoadAll("Localization", typeof(TextAsset)).Cast<TextAsset>().ToArray();

                var languageString = string.Empty;
                foreach (var textAsset in jsonTextAssets)
                {
                    foreach (var l in Localization.Languages)
                    {
                        
                        var n = JSON.Parse(textAsset.text);
                        for (var i = 0; i < n.Count; i++)
                        {
                            //go through the list and add the strings to the dictionary
                            if (n[i][l.Name.ToLower()] != null)
                            {
                                var key = n[i][0].ToString();
                                key = key.Replace("\"", "").ToUpper();
                                var value = n[i][l.Name.ToLower()].ToString();
                                value = value.Replace("\"", "");
                                foreach (var c in value)
                                {
                                    if (!languageString.Contains(c))
                                    {
                                        languageString += c;
                                    }
                                }
                            }
                        }
                    }
                }
                _text.text = languageString;
                Debug.Log(languageString);
            }
        }
    }
}
